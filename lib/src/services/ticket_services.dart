import 'dart:convert';

import 'package:thevent/src/model/ticket_model.dart';
import 'package:http/http.dart' as http;
import 'package:thevent/src/repository/auth_repository.dart';

final _url = "http://192.168.43.122:9000/api";
final AuthRepository authRepository = AuthRepository();

class TicketService {
  Future getTicketsDisponible() async {
    print("getTicketsDisponible");
    final token = await authRepository.hasToken();
    print(token);
    try {
      var reponse = await http.get('$_url/tickets/disponible', headers: {
        'Authorization': 'Bearer $token',
        'Accept': 'application/json'
      });

      var body = json.decode(reponse.body);
      var jsonTicket = Ticket.fromJson(body);
      return jsonTicket.tickets;
    } catch (e) {
      return e;
    }
  }

  Future getTicketsIndisponible() async {
    print("getTicketsIndisponible");
    final token = await authRepository.hasToken();
    try {
      var reponse = await http.get('$_url/tickets/indisponible', headers: {
        'Authorization': 'Bearer $token',
        'Accept': 'application/json'
      });

      var body = json.decode(reponse.body);
      var jsonTicket = Ticket.fromJson(body);
      return jsonTicket.tickets;
    } catch (e) {
      return e;
    }
  }
}
