class ResponseSucces {
  bool success;
  ResponseSucces({this.success});

  ResponseSucces.fromJsonMap(Map<String, dynamic> json){
    success = json['success'];
  }
}