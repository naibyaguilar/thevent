import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:thevent/src/model/event_model.dart';
import 'package:thevent/src/model/session_model.dart';
import 'package:thevent/src/repository/auth_repository.dart';
import 'package:thevent/src/services/response/response.dart';

final AuthRepository authRepository = AuthRepository();

class EventService {
  String _url = 'http://192.168.43.122:9000/api';

  static Future<List<Event>> getevn() async {
    try {
      final resp = await http.get('http://192.168.43.122:9000/api/event');
      List<Event> list = parseUsers(resp.body);
      return list;
    } catch (e) {
      return e;
    }
  }

  static List<Event> parseUsers(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed.map<Event>((json) => Event.fromJsonMap(json)).toList();
  }

  Future<EventDetail> getDetailEvent(int id, int user) async {
    try {
      final resp = await http.get('$_url/detail/$id/$user');
      var decodedData = json.decode(resp.body);
      print(decodedData);
      var detail = EventDetail.fromJsonMap(decodedData);
      return detail;
    } catch (e) {
      print(e);
      return e;
    }
  }

  Future<ResponseSucces> putPart(Map<String, dynamic> request) async {
    final resp = await http.put('$_url/presence',
        headers: {'content-type': 'application/json'},
        body: json.encode(request));
         print(1);
        print(resp);

    final decodedData = json.decode(resp.body);
     print(2);
    print(decodedData);
    final success = new ResponseSucces.fromJsonMap(decodedData);
    return success;
  }

   Future<Map<DateTime, List<dynamic>>> getSession() async{
    
    final resp = await http.get( '$_url/sessions' );
    final decodedData = json.decode(resp.body);

    final session = new Sessions.fromJsonCalendar(decodedData);
    //_events = groupBy(session.items, (obj) => obj['date']);

    return session.sessionMap;

  }

    Future<MyEventsDetail> getMyEventsDetail(int id) async {
    try {
      final resp = await http.get('$_url/myevents/detail/$id');
      final decodedData = json.decode(resp.body);
      final detail = MyEventsDetail.fromJsonMap(decodedData);
      return detail;
    } catch (e) {
      print("error");
      return e;
    }
  }
  
  Future getMyEvents() async {
    final token = await authRepository.hasToken();
    try {
      var reponse = await http.get('$_url/myevents', headers: {
        'Authorization': 'Bearer $token',
        'Accept': 'application/json'
      });
      var body = json.decode(reponse.body);
      var jsonEvents = MyEvents.fromJson(body);
      return jsonEvents.data;
    } catch (e) {
      return e;
    }
  }

  Future getMyParticipants(int id) async {
    try {
      final reponse = await http.get('$_url/event/participants/$id');
      var body = json.decode(reponse.body);
      var jsonParticipants = MyParticipants.fromJson(body);
      return jsonParticipants.data;
    } catch (e) {
      return e;
    }
  }

  Future getSessionsParticipant(int idEvent, int idParticipant) async {
    try {
      final reponse = await http.get('$_url/myevents/$idEvent/$idParticipant');
      var body = json.decode(reponse.body);
      var jsonSessionsParticipant = ParticipantsSessions.fromJson(body);
      return jsonSessionsParticipant.data;
    } catch (e) {
      return e;
    }
  }

  Future<ResponseSucces> putPresenceGeo(Map<String, int> request) async {
    final resp = await http.put('$_url/presence/valider/',
        headers: {'content-type': 'application/json'},
        body: json.encode(request));
         print(1);
        print(resp);

    final decodedData = json.decode(resp.body);
     print(2);
    print(decodedData);
    final success = new ResponseSucces.fromJsonMap(decodedData);
    return success;
  }
}
