import 'package:equatable/equatable.dart';
import 'package:thevent/src/model/data_model.dart';

abstract class AuthenticationState extends Equatable {
  AuthenticationState();

  @override
  List<Object> get props => [];
}

class AuthenticationInitial extends AuthenticationState {}

class AuthenticationSuccess extends AuthenticationState {
  final User user;

  AuthenticationSuccess({this.user});

  @override
  List<Object> get props => [user];
}

class AuthenticationRegisterSuccess extends AuthenticationState {
  final User user;

  AuthenticationRegisterSuccess({this.user});

  @override
  List<Object> get props => [user];
}

class AuthenticationFailure extends AuthenticationState {}

class AuthenticationHasToken extends AuthenticationState {
  final String token;
  AuthenticationHasToken({this.token});

  @override
  List<Object> get props => [token];
}