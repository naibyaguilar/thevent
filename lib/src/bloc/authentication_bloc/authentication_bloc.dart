import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thevent/src/bloc/authentication_bloc/authentication_event.dart';
import 'package:thevent/src/bloc/authentication_bloc/authentication_state.dart';
import 'package:thevent/src/model/data_model.dart';
import 'package:thevent/src/repository/auth_repository.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthRepository _authRepository;

  AuthenticationBloc({AuthRepository authRepository})
      : _authRepository = authRepository,
        super(AuthenticationInitial());

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    if (event is AuthenticationStarted) {
      print('auth bloc - AuthenticationStarted');
      final hasToken = await _authRepository.hasToken();
      if (hasToken != null) {
        final user = await _authRepository.getData(hasToken);
        yield AuthenticationSuccess(user: user);
      } else {
        yield AuthenticationFailure();
      }
    } else if (event is AuthenticationLoggedIn) {
      print('auth bloc - AuthenticationLoggedIn');
      final hasToken = await _authRepository.hasToken();
      final user = await _authRepository.getData(hasToken);
      yield AuthenticationSuccess(user: user);
    } else if (event is AuthenticationLoggedOut) {
      print('auth bloc - AuthenticationLoggedOut');
      final String token = await _authRepository.hasToken();
      final Logout logout = await _authRepository.userLogout(token);
      if (logout.message == "success") {
        await _authRepository.unsetLocalToken();
        yield AuthenticationFailure();
      }
    } else if (event is AuthenticationRegisterIn){
      print('auth bloc - AuthenticationRegisterIn');
      final hasToken = await _authRepository.hasToken();
      final user = await _authRepository.getData(hasToken);
      yield AuthenticationRegisterSuccess(user: user);
    }
/*
    if (event is LoginProcess) {
      yield AuthLoading();
      try {
        final login = await authRepository.loginUser(
            event.email, event.password, "Mobile Sanctum");
        if (login.message != "failed") {
          yield LoginSuccess();
          await authRepository.setLocalToken(login.data.token);
          yield AuthHasToken(token: login.data.token);
        }
      } catch (e) {
        yield LoginFailed("login gagal");
      }
    }

    if (event is LoggedOut) {
      final String token = await authRepository.hasToken();
      try {
        final Logout logout = await authRepository.userLogout(token);
        if (logout.message == "success") {
          await authRepository.unsetLocalToken();
          yield AuthFailed();
        }
      } catch (e) {}
    }*/
  }
}
