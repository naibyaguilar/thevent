import 'package:equatable/equatable.dart';

abstract class RegisterEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class RegisterSubmitted extends RegisterEvent {
  final String nom;
  final String prenom;
  final String telephone;
  final String email;
  final String password;

  RegisterSubmitted({this.nom, this.prenom, this.telephone, this.email, this.password});

  @override
  List<Object> get props => [nom, prenom, telephone, email, password];
}
