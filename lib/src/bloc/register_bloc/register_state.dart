
class RegisterState {
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;

  RegisterState({this.isSubmitting, this.isSuccess, this.isFailure});

  factory RegisterState.initial() {
    return RegisterState(
        isSubmitting: false, isSuccess: false, isFailure: false);
  }

  factory RegisterState.loading() {
    return RegisterState(
        isSubmitting: true, isSuccess: false, isFailure: false);
  }

  factory RegisterState.failure() {
    return RegisterState(
        isSubmitting: false, isSuccess: false, isFailure: true);
  }

  factory RegisterState.success() {
    return RegisterState(
        isSubmitting: false, isSuccess: true, isFailure: false);
  }
}
