import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thevent/src/bloc/register_bloc/register_state.dart';
import 'package:thevent/src/bloc/register_bloc/register_event.dart';
import 'package:thevent/src/repository/auth_repository.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final AuthRepository _authRepository;

  RegisterBloc({AuthRepository authRepository})
      : _authRepository = authRepository,
        super(RegisterState.initial());

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterSubmitted) {
      print('register bloc - RegisterSubmitted');
      yield RegisterState.loading();
      try {
        print('register bloc - try');
        final signup = await _authRepository.signUp(event.nom, event.prenom,
            event.telephone, event.email, event.password);
        print(signup);
        if (signup.message == "success") {
          final login = await _authRepository.loginUser(
              event.email, event.password, "Mobile Sanctum");
          yield RegisterState.success();
          await _authRepository.setLocalToken(login.data.token);
        }
        print(signup);
      } catch (error) {
        print('register bloc - catch');
        print(error);
        yield RegisterState.failure();
      }
    }
  }
}
