import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thevent/src/bloc/login_bloc/login_event.dart';
import 'package:thevent/src/bloc/login_bloc/login_state.dart';
import 'package:thevent/src/repository/auth_repository.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final AuthRepository _authRepository;

  LoginBloc({AuthRepository authRepository})
      : _authRepository = authRepository,
        super(LoginState.initial());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    print(event);
    if (event is LoginWithCredentialsPressed) {
      print('login bloc - LoginWithCredentialsPressed');

      yield LoginState.loading();
      try {
        print('login bloc - try');

        final login = await _authRepository.loginUser(
            event.email, event.password, "Mobile Sanctum");
        if (login.message == "success") {
          yield LoginState.success();
          await _authRepository.setLocalToken(login.data.token);
        } else {
          yield LoginState.failure();
        }
      } catch (e) {
        print('login bloc - catch');
        yield LoginState.failure();
      }
    }
  }
}
