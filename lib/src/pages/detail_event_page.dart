import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:thevent/src/animation/fade_animation.dart';
import 'package:thevent/src/model/event_model.dart';
import 'package:thevent/src/model/session_model.dart';
import 'package:thevent/src/model/data_model.dart';
import 'package:thevent/src/services/event_service.dart';
import 'package:thevent/src/services/response/response.dart';
import 'package:thevent/src/widgets/detaill_app_bar.dart';
import 'package:thevent/src/size/size_config.dart';
//

class DetailEvent extends StatefulWidget {
  int id;
  final User user;
  DetailEvent({Key key, @required this.id, this.user}) : super(key: key);

  @override
  _DetailEventState createState() => _DetailEventState(id);
}

class _DetailEventState extends State<DetailEvent> {
  User get _user => widget.user;
  List<int> ids = new List();
  int id;
  ResponseSucces resp;
  _DetailEventState(this.id);
  final detail = new EventService();
  Future<EventDetail> response;
  final image =
      'https://images.pexels.com/photos/814499/pexels-photo-814499.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500';

  @override
  void initState() {
    super.initState();
    response = detail.getDetailEvent(id, _user.id);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return FutureBuilder(
        future: response,
        builder: (context, AsyncSnapshot<EventDetail> snapshot) {
          if (snapshot.hasData) {
            return _listDetail(context, defaultSize, snapshot.data);
          } else {
            return Container(
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(
                  strokeWidth: 6,
                  valueColor:
                      new AlwaysStoppedAnimation<Color>(Colors.pinkAccent),
                ),
              ),
            );
          }
        });
  }

  Widget _listDetail(
      BuildContext context, double defaultSize, EventDetail detail) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: CustomScrollView(slivers: <Widget>[
          appBarDetail(context, defaultSize, detail.event.getImage(),
              detail.participants, detail.event.id),
          SliverList(
              delegate: SliverChildListDelegate([
            Padding(
                padding: EdgeInsets.all(defaultSize * 1),
                child: _content(defaultSize, detail))
          ]))
        ]));
  }

  Widget _content(double defaultSize, EventDetail detail) {
    return Column(
      children: [
        Container(
          alignment: AlignmentDirectional.topStart,
          margin: EdgeInsets.all(defaultSize * 2),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                detail.event.titre,
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: defaultSize * 2.4),
              ),
              Text(
                detail.event.categorie,
                style: TextStyle(
                    color: Colors.grey[600], fontSize: defaultSize * 1.8),
              ),
              Divider(),
              _iconsdetail(defaultSize, detail.event, detail.participants,
                  detail.createur[0]),
              SizedBox(height: defaultSize * 2.0),
              _description(defaultSize, detail.event.description),
              SizedBox(height: defaultSize * 2.0),
              Divider(),
              _sessions(defaultSize, detail.sessions),
              SizedBox(height: defaultSize * 2.0),
              _button(defaultSize),
              SizedBox(
                height: defaultSize * 2.0,
              ),
              //_createur(defaultSize, detail.createur[0]),
              SizedBox(height: defaultSize * 2.0),
            ],
          ),
        )
      ],
    );
  }

  Widget _iconsdetail(double defaultSize, Event event, int part, User user) {
    return Container(
        child: Column(
      children: [
        ListTile(
          leading: Container(
              child: Icon(Icons.calendar_today,
                  size: defaultSize * 3, color: Color(0xFF7900FF)),
              width: defaultSize * 4.0,
              height: defaultSize * 4.0,
              decoration: BoxDecoration(
                  color: Color(0xFFEBEBFC),
                  borderRadius: BorderRadius.all(Radius.circular(20.0)))),
          title: Text(formatDate(event.moonLanding(), [MM, ' ', d, ', ', yyyy]),
              style: TextStyle(
                  fontWeight: FontWeight.w400, fontSize: defaultSize * 1.8)),
        ),
        ListTile(
          leading: Container(
              child: Icon(Icons.place,
                  size: defaultSize * 3, color: Color(0xFF7900FF)),
              width: defaultSize * 4.0,
              height: defaultSize * 4.0,
              decoration: BoxDecoration(
                  color: Color(0xFFEBEBFC),
                  borderRadius: BorderRadius.all(Radius.circular(20.0)))),
          title: Text(event.location,
              style: TextStyle(
                  fontWeight: FontWeight.w400, fontSize: defaultSize * 1.8)),
        ),
        ListTile(
          leading: Container(
              child: Icon(Icons.people_alt,
                  size: defaultSize * 2, color: Color(0xFF7900FF)),
              width: defaultSize * 4.0,
              height: defaultSize * 4.0,
              decoration: BoxDecoration(
                  color: Color(0xFFEBEBFC),
                  borderRadius: BorderRadius.all(Radius.circular(20.0)))),
          title: Text('$part',
              style: TextStyle(
                  fontWeight: FontWeight.w400, fontSize: defaultSize * 1.8)),
        ),
      ],
    ));
  }

  Widget _description(double defaultSize, String description) {
    return Container(
        alignment: AlignmentDirectional.topStart,
        margin: EdgeInsets.only(left: defaultSize * 1.0),
        child: Column(
          children: [
            Container(
              alignment: AlignmentDirectional.topStart,
              child: Text(
                'Description :',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: defaultSize * 1.4,
                  color: Colors.grey,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(height: defaultSize * 1.0),
            Text(
              description,
              textAlign: TextAlign.left,
            ),
          ],
        ));
  }

  Widget _sessions(double defaultSize, List<Session> session) {
    return Container(
      alignment: AlignmentDirectional.topStart,
      margin: EdgeInsets.only(left: defaultSize * 1.0),
      child: Column(children: [
        Container(
          alignment: AlignmentDirectional.topStart,
          child: Text(
            'Sessions :',
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: defaultSize * 1.4,
              color: Colors.grey,
              fontWeight: FontWeight.w600,
            ),
          ),
        ),
        SizedBox(height: defaultSize * 1),
        ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: session.length,
            padding:
                EdgeInsets.only(left: 0.0, top: 0.0, right: 0.0, bottom: 0.0),
            itemBuilder: (context, i) {
              if (session[i].isSelected) {
                ids.add(session[i].id);
              }
              return __viewHolderSession(i, session, defaultSize);
            }),
      ]),
    );
  }

  Widget __viewHolderSession(
      int i, List<Session> sessions, double defaultSize) {
    return CheckboxListTile(
        controlAffinity: ListTileControlAffinity.leading,
        title: Text(
          sessions[i].titre,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
            fontSize: defaultSize * 1.8,
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        subtitle: Container(
          child: Column(
            children: [
              SizedBox(height: defaultSize * 0.5),
              Container(
                  alignment: AlignmentDirectional.topStart,
                  child: Text(
                    '${formatDate(sessions[i].moonLanding(), [
                      MM,
                      ' ',
                      d,
                      ', ',
                      yyyy,
                    ])}',
                    style: TextStyle(
                        fontSize: defaultSize * 1.5, color: Colors.grey[600]),
                  )),
              Row(
                children: [
                  Text(
                    '${formatDate(sessions[i].houreDebutLAN(), [
                      HH,
                      ':',
                      nn
                    ])}  -',
                    style: TextStyle(
                        fontSize: defaultSize * 1.4, color: Colors.grey),
                  ),
                  SizedBox(width: defaultSize * 1.0),
                  Text(
                    '${formatDate(sessions[i].houreFinLAN(), [HH, ':', nn])}',
                    style: TextStyle(
                        fontSize: defaultSize * 1.4, color: Colors.grey),
                  ),
                ],
              ),
              Divider()
            ],
          ),
        ),
        value: sessions[i].isSelected,
        onChanged: (value) {
          setState(() {
            sessions[i].isSelected = value;
            if (sessions[i].isSelected) {
                  ids.add(sessions[i].id);
            } else {
              ids.remove(sessions[i].id);
            }
          });
        });
  }

  Widget _button(double defaultSize) {
    return FadeAnimation(
        1.7,
        Container(
            child: Center(
          child: RaisedButton(
            padding: EdgeInsets.all(0.0),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            onPressed: () async {
              try {
                Map<String, dynamic> request = {
                  'user': _user.email,
                  'event': id,
                  'check_sessio': ids
                };
                resp = await detail.putPart(request);
                print(0);
                print(resp);
                if (resp.success) {
                  Navigator.pop(context);
                }
              } catch (e) {
                _onAlertErrorCreateur(context);
                print(e);
              }
            },
            child: Container(
              height: defaultSize * 5.0,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  gradient: LinearGradient(colors: [
                    Color.fromRGBO(49, 235, 228, 1),
                    Color.fromRGBO(136, 49, 235, 1),
                    Color.fromRGBO(253, 45, 248, 1),
                  ])),
              child: Center(
                child: Text(
                  "PARTICIPE",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
        )));
  }

  Widget _createur(double defaultSize, User user) {
    return Container(
        child: Row(
      children: [
        Container(
          width: defaultSize * 4.0,
          height: defaultSize * 4.0,
          decoration: BoxDecoration(
              color: Color(0xFFEBEBFC),
              image: DecorationImage(
                  image: NetworkImage(image), fit: BoxFit.cover),
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 18.0),
          child: Text('${user.nom} ${user.prenom}',
              style: TextStyle(
                  fontWeight: FontWeight.w400, fontSize: defaultSize * 2.0)),
        ),
      ],
    ));
  }

  _onAlertErrorCreateur(context) {
    Alert(
      context: context,
      type: AlertType.warning,
      title: "Vous devez choisir au moins une session.",
      style: AlertStyle(descStyle: TextStyle(fontSize: 14)),
      buttons: [
        DialogButton(
          child: Text(
            "D'ACCORD",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () => Navigator.pop(context),
          gradient: LinearGradient(colors: [
            Color.fromRGBO(136, 49, 235, 1),
            Color.fromRGBO(253, 45, 248, 1),
          ]),
        )
      ],
    ).show();
  }
}
