import 'package:flutter/material.dart';
import 'package:thevent/src/model/event_model.dart';
import 'package:thevent/src/pages/home/fragments/myevent_participants_fragment.dart';
import 'package:thevent/src/services/event_service.dart';
import 'package:thevent/src/size/size_config.dart';

class MyEventDetail extends StatefulWidget {
  final Event event;
  const MyEventDetail({Key key, this.event}) : super(key: key);
  @override
  _MyEventDetailState createState() => _MyEventDetailState();
}

class _MyEventDetailState extends State<MyEventDetail> {
  Event get _event => widget.event;
  final event = new EventService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: getBody(),
    );
  }

  Widget getBody() {
    var size = MediaQuery.of(context).size;
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    var sizedBox = SizedBox(
      height: 20,
    );
    return FutureBuilder(
        future: event.getMyEventsDetail(_event.id),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return CustomScrollView(
              slivers: [
                SliverList(
                  delegate: SliverChildListDelegate([
                    Stack(
                      children: [
                        Container(
                          width: double.infinity,
                          height: size.height * 0.5,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(_event.getImage()),
                                  fit: BoxFit.cover)),
                          child: SafeArea(
                            child: Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: defaultSize * 3,
                                  vertical: defaultSize * 3),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: defaultSize * 4,
                                    width: defaultSize * 4,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(100),
                                      color: Colors.black26,
                                    ),
                                    clipBehavior: Clip.antiAlias,
                                    child: IconButton(
                                      icon: Icon(Icons.arrow_back_ios_rounded,
                                          color: Colors.white,
                                          size: defaultSize * 2),
                                      onPressed: () {
                                        Navigator.pop(context, false);
                                      },
                                    ),
                                  ),
                                  Container(
                                    height: defaultSize * 4,
                                    width: defaultSize * 4,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(100),
                                      color: Colors.black26,
                                    ),
                                    clipBehavior: Clip.antiAlias,
                                    child: IconButton(
                                      icon: Icon(Icons.people_alt,
                                          color: Colors.white,
                                          size: defaultSize * 2),
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    MyEventParticipant(
                                                        id: _event.id,view: true)));
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                            width: double.infinity,
                            margin: EdgeInsets.only(top: size.height * 0.45),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(50)),
                            child: Padding(
                              padding: EdgeInsets.all(defaultSize * 3),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Align(
                                    child: Container(
                                      width: defaultSize * 15,
                                      height: defaultSize * 0.7,
                                      decoration: BoxDecoration(
                                          color: Colors.purple[100],
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                    ),
                                  ),
                                  Text(
                                    _event.titre,
                                    style: TextStyle(
                                        fontSize: defaultSize * 2.4,
                                        height: defaultSize * .3),
                                  ),
                                  SizedBox(
                                    height: defaultSize * 1,
                                  ),
                                  Row(children: [
                                    Icon(
                                      Icons.place,
                                      size: defaultSize * 2.4,
                                      color: Colors.purple,
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: Padding(
                                          padding: const EdgeInsets.all(6.0),
                                          child: Text(
                                            _event.location,
                                            style: TextStyle(
                                                fontSize: defaultSize * 1.6,
                                                color: Colors.black54),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ]),
                                  SizedBox(
                                    height: defaultSize * 2,
                                  ),
                                  Text(
                                    _event.description,
                                    style: TextStyle(height: defaultSize * .2),
                                  ),
                                  SizedBox(
                                    height: defaultSize * 3,
                                  ),
                                  Text(
                                    "Sessions",
                                    style: TextStyle(fontSize: defaultSize * 2),
                                  ),
                                  SizedBox(
                                    height: defaultSize * 2,
                                  ),
                                  Container(
                                    child: ListView.builder(
                                        scrollDirection: Axis.vertical,
                                        shrinkWrap: true,
                                        itemCount:
                                            snapshot.data.sessions.length,
                                        padding: EdgeInsets.only(
                                            left: 0.0,
                                            top: 0.0,
                                            right: 0.0,
                                            bottom: 0.0),
                                        itemBuilder: (context, i) {
                                          return ListTile(
                                            title: Text(
                                              snapshot.data.sessions[i].titre,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                fontSize: defaultSize * 1.8,
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                            subtitle: Column(
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: <Widget>[
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          "Date",
                                                          style: TextStyle(
                                                              fontSize:
                                                                  defaultSize *
                                                                      1.4,
                                                              color:
                                                                  Colors.grey),
                                                        ),
                                                        Text(
                                                          snapshot.data
                                                              .sessions[i].date,
                                                          style: TextStyle(
                                                              fontSize:
                                                                  defaultSize *
                                                                      1.6,
                                                              color: Colors
                                                                  .grey[600]),
                                                        )
                                                      ],
                                                    ),
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          "Debut",
                                                          style: TextStyle(
                                                              fontSize:
                                                                  defaultSize *
                                                                      1.4,
                                                              color:
                                                                  Colors.grey),
                                                        ),
                                                        Text(
                                                          snapshot
                                                              .data
                                                              .sessions[i]
                                                              .heureDebut,
                                                          style: TextStyle(
                                                              fontSize:
                                                                  defaultSize *
                                                                      1.6,
                                                              color: Colors
                                                                  .grey[600]),
                                                        )
                                                      ],
                                                    ),
                                                    Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          "Fin",
                                                          style: TextStyle(
                                                              fontSize:
                                                                  defaultSize *
                                                                      1.4,
                                                              color:
                                                                  Colors.grey),
                                                        ),
                                                        Text(
                                                          snapshot
                                                              .data
                                                              .sessions[i]
                                                              .heureFin,
                                                          style: TextStyle(
                                                              fontSize:
                                                                  defaultSize *
                                                                      1.6,
                                                              color: Colors
                                                                  .grey[600]),
                                                        )
                                                      ],
                                                    ),
                                                  ],
                                                ),
                                                Divider()
                                              ],
                                            ),
                                          );
                                        }),
                                  ),
                                ],
                              ),
                            ))
                      ],
                    ),
                  ]),
                )
              ],
            );
          } else {
            return Container(
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(
                  strokeWidth: 6,
                  valueColor:
                      new AlwaysStoppedAnimation<Color>(Colors.pinkAccent),
                ),
              ),
            );
          }
        });
  }
}
