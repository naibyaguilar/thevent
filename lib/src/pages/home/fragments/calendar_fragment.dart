import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:thevent/src/model/data_model.dart';
import 'package:thevent/src/pages/detail_event_page.dart';
import 'package:thevent/src/services/event_service.dart';
import 'package:thevent/src/widgets/nav_app_bar.dart';
import 'package:thevent/src/size/size_config.dart';

class CalendarFragment extends StatefulWidget {
  final User user;
  CalendarFragment({Key key, this.title, this.user}) : super(key: key);
  final String title;
  @override
  _CalendarFragmentState createState() => _CalendarFragmentState();
}

class _CalendarFragmentState extends State<CalendarFragment> with TickerProviderStateMixin {
  User get _user => widget.user;
  
  final detail = new EventService();
  //<Map<DateTime, List<dynamic>> response;
  
  Map<DateTime, List> _events;
  List _selectedEvents;
  AnimationController _animationController;
  CalendarController _calendarController;

  Future<String> getData() async {
    var response = await detail.getSession();
    this.setState(() { 
      _events = response;
      final _selectedDay = DateTime.now();
      _selectedEvents = _events[_selectedDay] ?? [];
    });
    
    return "ok";
  }
  @override
  void initState() {
    super.initState();
    getData();
    _calendarController = CalendarController();

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _animationController.forward();

  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  void _onDaySelected(DateTime day, List events, List holidays) {
    print('CALLBACK: _onDaySelected');
    setState(() {
      _selectedEvents = events;
    });
  }

  void _onVisibleDaysChanged(
      DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: _onVisibleDaysChanged');
  }

  void _onCalendarCreated(
      DateTime first, DateTime last, CalendarFormat format) {
    print('CALLBACK: _onCalendarCreated');
  }

  

  @override
  Widget build(BuildContext context) {
     SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Container(
      child: Stack(
        children:[ 
          backApp(defaultSize, ''),
          Padding(
            padding: const EdgeInsets.only(top:70.0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                _buildTableCalendar(defaultSize),
                SizedBox(height: defaultSize* .8),
                Expanded(child: _selectedEvents == null
            ? CircularProgressIndicator() : _buildEventList(defaultSize)),
              ]
        ),
          ),]
      ),
    );
  }

  Widget _buildTableCalendar(double defaultSize) {
    return TableCalendar(
      calendarController: _calendarController,
      events: _events,
      startingDayOfWeek: StartingDayOfWeek.monday,
      calendarStyle: CalendarStyle(
        selectedColor: Color(0xFF36B2EB), 
        todayColor: Color(0xFFEBEBFC), 
        markersColor: Color(0xFF7900FF),
        outsideDaysVisible: false,
      ),
      headerStyle: HeaderStyle(
        formatButtonTextStyle:
            TextStyle().copyWith(color: Colors.white, fontSize: defaultSize *1.5),
        formatButtonDecoration: BoxDecoration(
          color: Color(0xFFF138F5),
          borderRadius: BorderRadius.circular(16.0),
        ),
      ),
      onDaySelected: _onDaySelected,
      onVisibleDaysChanged: _onVisibleDaysChanged,
      onCalendarCreated: _onCalendarCreated,
    );
  }

  Widget _buildTableCalendarWithBuilders(double defaultSize) {
    return TableCalendar(
      locale: 'pl_PL',
      calendarController: _calendarController,
      events: _events,
      initialCalendarFormat: CalendarFormat.month,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.sunday,
      availableGestures: AvailableGestures.all,
      availableCalendarFormats: const {
        CalendarFormat.month: '',
        CalendarFormat.week: '',
      },
      calendarStyle: CalendarStyle(
        outsideDaysVisible: false,
        weekendStyle: TextStyle().copyWith(color:  Color(0xFF7900FF)),
        holidayStyle: TextStyle().copyWith(color: Color(0xFF7900FF)),
      ),
      daysOfWeekStyle: DaysOfWeekStyle(
        weekendStyle: TextStyle().copyWith(color: Colors.blue[600]),
      ),
      headerStyle: HeaderStyle(
        centerHeaderTitle: true,
        formatButtonVisible: false,
      ),
      builders: CalendarBuilders(
        selectedDayBuilder: (context, date, _) {
          return FadeTransition(
            opacity: Tween(begin: 0.0, end: 1.0).animate(_animationController),
            child: Container(
              margin: const EdgeInsets.all(4.0),
              padding: const EdgeInsets.only(top: 5.0, left: 6.0),
              color: Color(0xFF998DDB),
              width: defaultSize*10.0,
              height: defaultSize* 10.0,
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: defaultSize*1.6),
              ),
            ),
          );
        },
        todayDayBuilder: (context, date, _) {
          return Container(
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(top: 5.0, left: 6.0),
            color: Color(0xFF7900FF),
            width: defaultSize *10.0, 
            height: defaultSize *10.0,
            child: Text(
              '${date.day}',
              style: TextStyle().copyWith(fontSize: defaultSize*1.6),
            ),
          );
        },
        markersBuilder: (context, date, events, holidays) {
          final children = <Widget>[];

          if (events.isNotEmpty) {
            children.add(
              Positioned(
                right: defaultSize*.1,
                bottom: defaultSize*.1,
                child: _buildEventsMarker(date, events,defaultSize),
              ),
            );
          }

          return children;
        },
      ),
      onDaySelected: (date, events, holidays) {
        _onDaySelected(date, events, holidays);
        _animationController.forward(from: 0.0);
      },
      onVisibleDaysChanged: _onVisibleDaysChanged,
      onCalendarCreated: _onCalendarCreated,
    );
  }

  Widget _buildEventsMarker(DateTime date, List events, double defaultSize) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: _calendarController.isSelected(date)
            ? Colors.brown[500]
            : _calendarController.isToday(date)
                ? Colors.brown[300]
                : Colors.blue[400],
      ),
      width: defaultSize* 1.6,
      height: defaultSize*1.6,
      child: Center(
        child: Text(
          '${events.length}',
          style: TextStyle().copyWith(
            color: Colors.white,
            fontSize: defaultSize* 1.2,
          ),
        ),
      ),
    );
  }

 
  Widget _buildEventList(double defaultSize) {
    return ListView(
      children: _selectedEvents
          .map((event) => Container(
                decoration: BoxDecoration(
                  border: Border.all(width:defaultSize* .08),
                  borderRadius: BorderRadius.circular(12.0),
                ),
                margin:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                child: ListTile(
                  title: Text(event['titre']),
                  subtitle: Text("Heure debut : ${event['heure_debut']} - Heure fin : ${event['heure_fin']} "),
                  onTap: () {
                    print(_user.email);
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailEvent(id: event['event'], user: _user)));
                  } ,
                ),
              ))
          .toList(),
    );
  }
}
