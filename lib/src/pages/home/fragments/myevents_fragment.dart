import 'package:flutter/material.dart';
import 'package:thevent/src/pages/home/fragments/myevent_detail_fragment.dart';
import 'package:thevent/src/services/event_service.dart';
import 'package:thevent/src/shimmer/shimmer_myevents.dart';
import 'package:thevent/src/size/size_config.dart';
import 'package:thevent/src/widgets/nav_app_bar.dart';

class MyEvents extends StatefulWidget {
  @override
  _MyEventsState createState() => _MyEventsState();
}

class _MyEventsState extends State<MyEvents> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: backApp(defaultSize, ''),
        elevation: 2,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        centerTitle: true,
        title: Text("MES ÉVÉNEMENTS",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: defaultSize * 2.8,
                color: Colors.black)),
      ),
      body: Container(
        child: Events(),
      ),
    );
  }
}

class Events extends StatefulWidget {
  @override
  EventsState createState() => EventsState();
}

class EventsState extends State<Events> {
  final event = new EventService();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: event.getMyEvents(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return GridView.builder(
                itemCount: snapshot.data.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2),
                itemBuilder: (BuildContext context, int index) {
                  return EventCard(
                    titre: snapshot.data[index].titre,
                    image: snapshot.data[index].getImage(),
                    event: snapshot.data[index]
                  );
                });
          } else {
            return ShimmerMyEvents();
          }

        });
  }
}

class EventCard extends StatelessWidget {
  final titre;
  final image;
  final event;
  EventCard({this.titre, this.image, this.event});

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      margin: EdgeInsets.all(10),
      child: Hero(
        tag: titre,
        child: Material(
          child: InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyEventDetail(event: event)));
            },
            child: GridTile(
              footer: Container(
                color: Colors.black54,
                child: ListTile(
                  title: Text(titre,
                      style: TextStyle(
                          fontSize: defaultSize * 1.6,
                          fontWeight: FontWeight.bold,
                          color: Colors.white)),
                ),
              ),
              child: Image.network(
                image,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
