import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:thevent/src/data/choice_data.dart';
import 'package:thevent/src/model/choice_model.dart';
import 'package:thevent/src/model/data_model.dart';
import 'package:thevent/src/model/event_model.dart';
import 'package:thevent/src/services/event_service.dart';
import 'package:thevent/src/utils/icon_string_util.dart';
import 'package:thevent/src/widgets/events_cardview.dart';
import 'package:thevent/src/widgets/nav_app_bar.dart';
import 'package:thevent/src/size/size_config.dart';
//

class HomeFragment extends StatefulWidget {
  final User user;
  HomeFragment({Key key, this.user}) : super(key: key);
  @override
  createState() => _HomePageState();
}

class _HomePageState extends State<HomeFragment> {
  List<Event> events = new List();
  List<Event> filteredEvents = new List();
  final double spacing = 8;
  List<ChoiceChipData> choiceChips = ChoiceChips.all;
  ChoiceChipData chipSelected;

  @override
  void initState() {
    super.initState();
    EventService.getevn().then((eventsFromServer) {
      setState(() {
        events = eventsFromServer;
        filteredEvents = events;
      });
    });
  }

  User get _user => widget.user;
  //final event = new EventService();
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;

    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Stack(
        children: <Widget>[
          backApp(defaultSize, ''),
          Container(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: defaultSize * 8),
                  child: _seachr(defaultSize),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: defaultSize * 0,
                      left: defaultSize * 1,
                      right: defaultSize * 1),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: buildChoiceChips(defaultSize),
                  ),
                ),
                SizedBox(height: defaultSize * 0.5),
                _list(filteredEvents),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buildChoiceChips(double defaultSize) => Wrap(
        runSpacing: spacing,
        spacing: spacing,
        children: choiceChips
            .map((choiceChip) => ChoiceChip(
                  avatar: getIcon(
                      choiceChip.icon,
                      choiceChip.isSelected
                          ? Color(0xFFEBEBFC)
                          : Color(0xFF998DDB)),
                  label: Text(choiceChip.label),
                  labelPadding: EdgeInsets.symmetric(
                      horizontal: defaultSize * 1, vertical: defaultSize * 0.3),
                  labelStyle: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: choiceChip.isSelected
                          ? Color(0xFFEBEBFC)
                          : Color(0xFF998DDB)),
                  onSelected: (isSelected) => setState(() {
                    chipSelected = choiceChip;
                    if (chipSelected.route != "Tout") {
                      filteredEvents = events
                          .where((element) =>
                              (element.categorie.contains(chipSelected.route)))
                          .toList();
                    } else {
                      filteredEvents = events;
                    }
                    print(chipSelected.route);
                    choiceChips = choiceChips.map((otherChip) {
                      final newChip = otherChip.copy(isSelected: false);
                      return choiceChip == newChip
                          ? newChip.copy(isSelected: isSelected)
                          : newChip;
                    }).toList();
                  }),
                  selected: choiceChip.isSelected,
                  elevation: 1,
                  selectedColor: Color(0xFF7900FF),
                  backgroundColor: Color(0xFFEBEBFC),
                ))
            .toList(),
      );

  Widget _list(List<Event> filteredEvents) {
    print(filteredEvents);

    SizeConfig().init(context);
    return EventCardView(event: filteredEvents, user: _user);
    /*return FutureBuilder(
      future: event.getEvent(),
      builder: (context, AsyncSnapshot<List> snapshot) {
        if (snapshot.hasData) {
          return EventCardView(event: snapshot.data, user: _user);
        } else {
          return ShimmerEvenement();
        }
      },
    );*/
  }

  Widget _seachr(double defaultSize) {
    return Container(
      child: TextField(
        autofocus: false,
        style: TextStyle(fontSize: defaultSize * 2),
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color.fromRGBO(255, 255, 255, 255)),
              borderRadius: BorderRadius.circular(20.0)),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Color.fromRGBO(255, 255, 255, 255)),
            borderRadius: BorderRadius.circular(20.0),
          ),
          hintText: 'Rechercher...',
          prefixIcon:
              Icon(Icons.search, color: Color.fromRGBO(18, 10, 110, 55)),
        ),
        onChanged: (string) {
          setState(() {
            filteredEvents = events
                .where((element) => (element.titre
                    .toLowerCase()
                    .contains(string.toLowerCase())))
                .toList();
          });
        },
      ),
    );
  }

  Widget _filter() {
    return Container(
      child: IconButton(
        icon: Icon(Icons.filter_list_alt),
        color: Color.fromRGBO(18, 10, 110, 55),
        onPressed: () {},
      ),
    );
  }
}

class ShimmerEvenement extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Shimmer.fromColors(
            highlightColor: Colors.white,
            baseColor: Colors.grey[300],
            child: ShimmerLayout(),
            period: Duration(milliseconds: 800),
          ),
        ],
      ),
    );
  }
}

class ShimmerLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Container(
      child: Column(
        children: [
          SizedBox(height: defaultSize * 1),
          Container(
            height: defaultSize * 22.0,
            width: defaultSize * 38,
            margin: EdgeInsets.only(
                left: defaultSize * 1,
                top: defaultSize * 1,
                right: defaultSize * 1,
                bottom: defaultSize * 1),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.grey,
            ),
          ),
          Container(
            height: defaultSize * 22.0,
            width: defaultSize * 38,
            margin: EdgeInsets.only(
                left: defaultSize * 1,
                top: defaultSize * 1,
                right: defaultSize * 1,
                bottom: defaultSize * 1),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.grey,
            ),
          ),
        ],
      ),
    );
  }
}

class CustomShape extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    double height = size.height;
    double width = size.width;
    path.lineTo(0, height - 100);
    path.quadraticBezierTo(width / 2, height, width, height - 100);
    path.lineTo(width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
