import 'package:flutter/material.dart';
import 'package:thevent/src/services/event_service.dart';
import 'package:thevent/src/shimmer/shimmer_participants.dart';
import 'package:thevent/src/size/size_config.dart';
import 'package:thevent/src/widgets/nav_app_bar.dart';

class MyEventParticipant extends StatefulWidget {
  final int id;
  final bool view;

  MyEventParticipant({Key key, this.id, this.view}) : super(key: key);
  @override
  _MyEventParticipantState createState() => _MyEventParticipantState(view);
}

class _MyEventParticipantState extends State<MyEventParticipant> {
  int get _id => widget.id;
  final event = new EventService();
  bool view;
  _MyEventParticipantState(this.view);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Scaffold(
      appBar: AppBar(
        flexibleSpace: backApp(defaultSize, ''),
        elevation: 2,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        centerTitle: true,
        title: Text("PARTICIPANTS",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: defaultSize * 2.8,
                color: Colors.black)),
      ),
      body: FutureBuilder(
          future: event.getMyParticipants(_id),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.separated(
                separatorBuilder: (context, index) => Divider(
                  indent: defaultSize * 8,
                  endIndent: defaultSize * 1,
                  thickness: 1,
                ),
                itemCount: snapshot.data.length,
                itemBuilder: (_, index) {
                  return ListTile(
                    title: Container(
                        child: Text(
                            snapshot.data[index].nom +
                                " " +
                                snapshot.data[index].prenom,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: defaultSize * 1.8))),
                    leading: Container(
                        child: CircleAvatar(
                      backgroundImage:
                          AssetImage("assets/images/profile_image.png"),
                    )),
                    trailing: Container(
                      child: IconButton(
                        icon: Icon(
                          Icons.search,
                          size: defaultSize * 2.5,
                          color: Colors.grey[600],
                        ),
                        onPressed: () {
                          if (view){
                            _modalBottomSheetMenu(
                              snapshot.data[index].nom,
                              snapshot.data[index].prenom,
                              snapshot.data[index].telephone,
                              snapshot.data[index].id);
                          }
                          
                        },
                      ),
                    ) ,
                    subtitle: Container(
                        child: Text(
                          snapshot.data[index].telephone,
                          style: TextStyle(
                              fontSize: defaultSize * 1.6, color: Colors.grey),
                        )),
                  );
                },
              );
            } else {
              return ShimmerParticipants();
            }
          }),
    );
  }

  void _modalBottomSheetMenu(
      String nom, String prenom, String telephone, int idParticipant) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return Container(
            height: defaultSize * 40,
            color: Colors.transparent, //could change this to Color(0xFF737373),
            //so you don't have to change MaterialApp canvasColor
            child: Container(
                child: Padding(
              padding: const EdgeInsets.all(15),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      children: <Widget>[
                        Container(
                          width: defaultSize * 8,
                          height: defaultSize * 8,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: AssetImage(
                                      "assets/images/profile_image.png"),
                                  fit: BoxFit.cover)),
                        ),
                        SizedBox(
                          width: defaultSize * 2,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              nom + " " + prenom,
                              style: TextStyle(
                                  fontSize: defaultSize * 2,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 3,
                            ),
                            Text(
                              telephone,
                              style: TextStyle(fontSize: defaultSize * 1.6),
                            )
                          ],
                        )
                      ],
                    ),
                    Divider(
                      thickness: 1,
                    ),
                    Text(
                      "Sessions inscrits",
                      style: TextStyle(
                          color: Colors.black, fontSize: defaultSize * 2),
                    ),
                    FutureBuilder(
                        future:
                            event.getSessionsParticipant(_id, idParticipant),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: defaultSize * 4,
                                  vertical: defaultSize * 2),
                              child: ListView.builder(
                                  scrollDirection: Axis.vertical,
                                  shrinkWrap: true,
                                  itemCount: snapshot.data.length,
                                  padding: EdgeInsets.only(
                                      left: 0.0,
                                      top: 0.0,
                                      right: 0.0,
                                      bottom: 0.0),
                                  itemBuilder: (context, i) {
                                    return ListTile(
                                      title: Text(
                                        snapshot.data[i].titre,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          fontSize: defaultSize * 1.8,
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      trailing: Container(
                                        child: (snapshot.data[i].validation == 0)
                                        ?
                                        Icon(
                                          Icons.remove_circle_rounded,
                                          color: Colors.grey,
                                        )
                                        :
                                        Icon(
                                          Icons.check_circle_rounded,
                                          color: Colors.green[600],
                                        ),
                                      ),
                                      subtitle: Column(
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Date",
                                                    style: TextStyle(
                                                        fontSize:
                                                            defaultSize * 1.4,
                                                        color: Colors.grey),
                                                  ),
                                                  Text(
                                                    snapshot.data[i].date,
                                                    style: TextStyle(
                                                        fontSize:
                                                            defaultSize * 1.6,
                                                        color:
                                                            Colors.grey[600]),
                                                  )
                                                ],
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Debut",
                                                    style: TextStyle(
                                                        fontSize:
                                                            defaultSize * 1.4,
                                                        color: Colors.grey),
                                                  ),
                                                  Text(
                                                    snapshot.data[i].heureDebut,
                                                    style: TextStyle(
                                                        fontSize:
                                                            defaultSize * 1.6,
                                                        color:
                                                            Colors.grey[600]),
                                                  )
                                                ],
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Fin",
                                                    style: TextStyle(
                                                        fontSize:
                                                            defaultSize * 1.4,
                                                        color: Colors.grey),
                                                  ),
                                                  Text(
                                                    snapshot.data[i].heureFin,
                                                    style: TextStyle(
                                                        fontSize:
                                                            defaultSize * 1.6,
                                                        color:
                                                            Colors.grey[600]),
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                          Divider()
                                        ],
                                      ),
                                    );
                                  }),
                            );
                          } else {
                            return Container(
                              height: defaultSize * 20,
                              color: Colors
                                  .transparent, //could change this to Color(0xFF737373),
                              child: Center(
                                child: CircularProgressIndicator(
                                  strokeWidth: 4,
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                      Colors.purpleAccent),
                                ),
                              ),
                            );
                          }
                        }),
                  ],
                ),
              ),
            )),
          );
        });
  }
}
