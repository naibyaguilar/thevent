import 'package:flutter/material.dart';
import 'package:thevent/src/widgets/tickets_avenir_listview.dart';
import 'package:thevent/src/widgets/tickets_passes_listview.dart';
import 'package:thevent/src/size/size_config.dart';
import 'package:thevent/src/widgets/nav_app_bar.dart';

class TicketFragment extends StatefulWidget {
  @override
  _TicketFragmentState createState() => _TicketFragmentState();
}

class _TicketFragmentState extends State<TicketFragment>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return new Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        flexibleSpace: backApp(defaultSize, 'TICKETS'),
        bottom: TabBar(
            controller: tabController,
            labelColor: Color.fromRGBO(18, 22, 64, 1),
            unselectedLabelColor: Color.fromRGBO(18, 22, 64, 0.5),
            indicatorColor: Color(0xFF7900FF),
            tabs: [
              Tab(
                text: "Disponible",
              ),
              Tab(
                text: "Indisponible",
              ),
            ]),
      ),
      body: TabBarView(controller: tabController, children: [
        TicketAvenirListView(),
        TicketPassesListView(),
      ]),
    ); //_buildListView(context));
  }
}
