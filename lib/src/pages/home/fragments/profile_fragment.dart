import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thevent/src/bloc/authentication_bloc/authentication_bloc.dart';
import 'package:thevent/src/bloc/authentication_bloc/authentication_event.dart';
import 'package:thevent/src/model/data_model.dart';
import 'package:thevent/src/pages/home/fragments/myevents_fragment.dart';
import 'package:thevent/src/size/size_config.dart';

class ProfileFragment extends StatefulWidget {
  final User user;

  const ProfileFragment({Key key, this.user}) : super(key: key);

  @override
  _ProfileFragmentState createState() => _ProfileFragmentState();
}

class _ProfileFragmentState extends State<ProfileFragment> {
  User get _user => widget.user;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    //return ShimmerProfile();
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Color.fromRGBO(18, 22, 64, 1),
          elevation: 0,
          title: Text("Profile"),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: defaultSize * 18, // 240
                child: Stack(
                  children: <Widget>[
                    ClipPath(
                      clipper: CustomShape(),
                      child: Container(
                        height: defaultSize * 15, //150
                        color: Color.fromRGBO(18, 22, 64, 1),
                        child: Image.asset(
                          'assets/images/background_profile.png',
                          width: defaultSize * 42,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(bottom: defaultSize),
                            height: defaultSize * 14,
                            width: defaultSize * 14,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                    color: Colors.white,
                                    width: defaultSize * 0.8),
                                image: DecorationImage(
                                    image: AssetImage(
                                        "assets/images/profile_image.png"),
                                    fit: BoxFit.cover)),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Center(
                child: Text(_user.nom + ' ' + _user.prenom,
                    style: TextStyle(
                      fontSize: defaultSize * 2.2,
                      color: Colors.grey[800],
                    )),
              ),
              SizedBox(
                height: defaultSize * 2,
              ),
              Column(children: [
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Text(
                            "10",
                            style: TextStyle(fontSize: defaultSize * 1.4),
                          ),
                          Text(
                            "Inscrits",
                            style: TextStyle(fontSize: defaultSize * 1.6),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Text(
                            "5",
                            style: TextStyle(fontSize: defaultSize * 1.4),
                          ),
                          Text(
                            "Crées",
                            style: TextStyle(fontSize: defaultSize * 1.6),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ]),
              SizedBox(
                height: defaultSize * 3,
              ),
              Container(
                  margin: EdgeInsets.symmetric(
                      vertical: 0, horizontal: defaultSize * 2),
                  child: Center(
                    child: RaisedButton(
                      elevation: 1,
                      padding: EdgeInsets.all(0.0),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(color: Colors.grey[300], width: 0.5)),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MyEvents()));
                      },
                      color: Colors.white,
                      child: Container(
                        height: defaultSize * 4,
                        child: Center(
                          child: Text("Voir mes événements",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400)),
                        ),
                      ),
                    ),
                  )),
              Card(
                elevation: 1,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: BorderSide(color: Colors.grey[300], width: 0.5)),
                margin: EdgeInsets.all(defaultSize * 2),
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Text(
                        "Information",
                        style: TextStyle(fontSize: defaultSize * 1.8),
                      ),
                    ),
                    Divider(),
                    ListTile(
                      title: Text(
                        "E-mail",
                        style: TextStyle(fontSize: defaultSize * 1.6),
                      ),
                      subtitle: Text(
                        _user.email,
                        style: TextStyle(fontSize: defaultSize * 1.4),
                      ),
                      leading: Icon(
                        Icons.email,
                        size: defaultSize * 3,
                      ),
                    ),
                    ListTile(
                      title: Text(
                        "Télephone",
                        style: TextStyle(fontSize: defaultSize * 1.6),
                      ),
                      subtitle: Text(
                        _user.telephone,
                        style: TextStyle(fontSize: defaultSize * 1.4),
                      ),
                      leading: Icon(
                        Icons.phone,
                        size: defaultSize * 3,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: defaultSize * 2),
                  width: defaultSize * 25,
                  height: defaultSize * 4,
                  child: Center(
                    child: RaisedButton(
                      padding: EdgeInsets.all(0.0),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      onPressed: () {
                        BlocProvider.of<AuthenticationBloc>(context)
                            .add(AuthenticationLoggedOut());
                      },
                      child: Container(
                        height: defaultSize * 5,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.red[800]),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.logout,
                                color: Colors.white,
                              ),
                              SizedBox(
                                width: defaultSize * 1,
                              ),
                              Text(
                                "SE DECONNECTER",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.normal),
                              ),
                            ]),
                      ),
                    ),
                  )),
              SizedBox(
                height: defaultSize * 3,
              ),
            ],
          ),
        ));
  }
}

class CustomShape extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    double height = size.height;
    double width = size.width;
    path.lineTo(0, height - 100);
    path.quadraticBezierTo(width / 2, height, width, height - 100);
    path.lineTo(width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
