import 'dart:ui';
import 'dart:convert';
import 'package:geolocator/geolocator.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:barcode_widget/barcode_widget.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:thevent/src/model/ticket_model.dart';
import 'package:thevent/src/services/event_service.dart';
import 'package:thevent/src/services/response/response.dart';
import 'package:thevent/src/services/ticket_services.dart';
import 'package:thevent/src/size/size_config.dart';

class DetailTicket extends StatefulWidget {
  final TicketModel ticketModel;

  DetailTicket({this.ticketModel});

  @override
  _DetailTicketState createState() => _DetailTicketState();
}

class _DetailTicketState extends State<DetailTicket> {
  ResponseSucces resp;
  final detail = new EventService();
  Future<ResponseSucces> response;
  TicketModel get ticketModel => widget.ticketModel;
  final ticket = new TicketService();
  double distance;
  Position _currentPosition;
  _getCurrentLocation() async {
    final geolocator = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best);
    setState(() {
      _currentPosition = geolocator;
    });
    print(geolocator);
  }

  @override
  void initState() {
    super.initState();
    _getCurrentLocation();
  }

  @override
  Widget build(BuildContext context) {
    var encoded = json.encode([
      {
        "createur": ticketModel.createur,
        "disponible": ticketModel.disponible,
        "valid": ticketModel.validation,
        "participant": ticketModel.participant,
        'session': ticketModel.session,
        "nom": ticketModel.nom,
        "prenom": ticketModel.prenom
      }
    ]);
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          color: Color.fromRGBO(18, 22, 64, 1),
          iconSize: defaultSize * 3,
          icon: Icon(Icons.keyboard_arrow_left_sharp),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          'My Ticket Event',
          style: TextStyle(
            color: Color.fromRGBO(18, 22, 64, 1),
          ),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Card(
              margin: EdgeInsets.all(defaultSize * 2.5),
              clipBehavior: Clip.antiAlias,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12)),
              elevation: 4,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Stack(
                    alignment: Alignment.bottomLeft,
                    children: [
                      CachedNetworkImage(
                        imageUrl: ticketModel.getImage(),
                        imageBuilder: (context, imageProvider) => Container(
                            height: defaultSize * 20,
                            width: defaultSize * 40,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  fit: BoxFit.fitWidth, image: imageProvider),
                              borderRadius: BorderRadius.circular(10.0),
                            )),
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.all(defaultSize * 2),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ListTile(
                          contentPadding: EdgeInsets.all(0),
                          title: Container(
                            child: Text(
                              ticketModel.titreSession,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: defaultSize * 2.4,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          subtitle: Container(
                            child: Text(
                              ticketModel.eventTitre,
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: defaultSize * 1.8,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          trailing: Container(
                            child: Text(
                              ticketModel.date,
                              style: TextStyle(
                                  color: Colors.black45,
                                  fontSize: defaultSize * 1.8,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Divider(),
                        Container(
                            child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Heure Début",
                                  style: TextStyle(
                                      fontSize: defaultSize * 1.4,
                                      color: Colors.grey),
                                ),
                                SizedBox(
                                  height: defaultSize * 1,
                                ),
                                Text(
                                  ticketModel.heureDebut,
                                  style: TextStyle(
                                    fontSize: defaultSize * 1.6,
                                    color: Colors.black,
                                  ),
                                  overflow: TextOverflow.visible,
                                  softWrap: true,
                                )
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Heure Fin",
                                  style: TextStyle(
                                      fontSize: defaultSize * 1.4,
                                      color: Colors.grey),
                                ),
                                SizedBox(
                                  height: defaultSize * 1,
                                ),
                                Text(
                                  ticketModel.heureFin,
                                  style: TextStyle(
                                    fontSize: defaultSize * 1.6,
                                    color: Colors.black,
                                  ),
                                  overflow: TextOverflow.visible,
                                  softWrap: true,
                                )
                              ],
                            ),
                          ],
                        )),
                        SizedBox(
                          height: defaultSize * 1,
                        ),
                        Container(
                          margin: EdgeInsets.only(top: defaultSize * 1),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Localitation",
                                style: TextStyle(
                                    fontSize: defaultSize * 1.4,
                                    color: Colors.grey),
                              ),
                              SizedBox(
                                height: defaultSize * 1,
                              ),
                              Text(
                                ticketModel.location,
                                style: TextStyle(
                                  fontSize: defaultSize * 1.6,
                                  color: Colors.black,
                                ),
                                overflow: TextOverflow.visible,
                                softWrap: true,
                              )
                            ],
                          ),
                        ),
                        Divider(),
                        Text(
                          "QR",
                          style: TextStyle(
                              fontSize: defaultSize * 1.4, color: Colors.grey),
                        ),
                        SizedBox(
                          height: defaultSize * 1,
                        ),
                        Center(
                            child: Column(
                          children: [
                            BarcodeWidget(
                              data: encoded,
                              color: Colors.black,
                              height: defaultSize * 20,
                              width: defaultSize * 20,
                              barcode: Barcode.qrCode(),
                            ),
                          ],
                        )),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            _botonGeo(defaultSize),
            SizedBox(height: defaultSize * .5),
            Text("Vous devez être présent à l'adresse de l'événement",
                style: TextStyle(color: Colors.red[300], fontWeight: FontWeight.w800)),
                    
            SizedBox(height: defaultSize * 5),
          ],
        ),
      ),
    );
  }

  Widget _botonGeo(double defaultSize) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 0, horizontal: defaultSize * 2),
        child: Center(
          child: RaisedButton(
            elevation: 1,
            padding: EdgeInsets.all(0.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
                side: BorderSide(color: Colors.grey[300], width: 0.5)),
            onPressed: () async {
              Map<String, int> request = {
                'participant': int.tryParse(ticketModel.participant.toString()),
                'session': int.tryParse(ticketModel.session.toString())
              };

              if (_currentPosition != null) {
                distance = Geolocator.distanceBetween(
                    _currentPosition.latitude,
                    _currentPosition.longitude,
                    double.parse(ticketModel.lat),
                    double.parse(ticketModel.lng));
                if (distance <= 20.0) {
                  try {
                    resp = await detail.putPresenceGeo(request);
                    _onAlertSucessValid(context, ticketModel.nom.toString(),
                        ticketModel.prenom.toString());
                  } catch (e) {
                    print(e.toString());
                  }
                } else {
                  _onAlertError(context);
                }
              }
            },
            color: Colors.white,
            child: Container(
              height: defaultSize * 4,
              child: Center(
                child: Row(
                  children: [
                    SizedBox(width: defaultSize * 8),
                    Icon(Icons.location_on),
                    SizedBox(
                      width: defaultSize * 4,
                    ),
                    Text(
                      "Geo-Presences",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  Widget FadeAlertAnimation(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return Align(
      child: FadeTransition(
        opacity: animation,
        child: child,
      ),
    );
  }

  _onAlertSucessValid(context, String nom, String prenom) {
    Alert(
      context: context,
      type: AlertType.success,
      title: nom + " " + prenom + " validée",
      desc:
          "Pour afficher les utilisateurs validés, ils se trouvent dans vos événements.",
      style: AlertStyle(descStyle: TextStyle(fontSize: 14)),
      buttons: [
        DialogButton(
          child: Text(
            "D'ACCORD",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () => Navigator.pop(context),
          gradient: LinearGradient(colors: [
            Color.fromRGBO(136, 49, 235, 1),
            Color.fromRGBO(253, 45, 248, 1),
          ]),
        )
      ],
      alertAnimation: FadeAlertAnimation,
    ).show();
  }

  _onAlertError(context) {
    Alert(
      context: context,
      type: AlertType.error,
      title: "Votre ubication n'est pas valable",
      desc: "Vous devez être présent à l'adresse de l'événement, vérifie bien.",
      style: AlertStyle(descStyle: TextStyle(fontSize: 14)),
      buttons: [
        DialogButton(
          child: Text(
            "D'ACCORD",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () => Navigator.pop(context),
          gradient: LinearGradient(colors: [
            Color.fromRGBO(136, 49, 235, 1),
            Color.fromRGBO(253, 45, 248, 1),
          ]),
        )
      ],
      alertAnimation: FadeAlertAnimation,
    ).show();
  }
}
