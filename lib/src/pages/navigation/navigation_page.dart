import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:spincircle_bottom_bar/modals.dart';
import 'package:spincircle_bottom_bar/spincircle_bottom_bar.dart';
import 'package:thevent/src/model/data_model.dart';
import 'dart:ui';
import 'dart:convert';
import 'package:http/http.dart' as http;

//
import 'package:thevent/src/pages/home/fragments/home_fragment.dart';
import 'package:thevent/src/pages/home/fragments/calendar_fragment.dart';
import 'package:thevent/src/pages/home/fragments/profile_fragment.dart';
import 'package:thevent/src/pages/home/fragments/ticket_fragment.dart';
import 'package:thevent/src/widgets/mapa_sessions.dart';

class NavigationPage extends StatefulWidget {
  final User user;

  const NavigationPage({Key key, this.user}) : super(key: key);

  @override
  createState() => NavigationState();
}

class NavigationState extends State<NavigationPage> {
  User get _user => widget.user;

  String qrString = "Not Scanned";
  Future<void> scanQR() async {
    try {
      FlutterBarcodeScanner.scanBarcode("#2A99CF", "Cancel", true, ScanMode.QR)
          .then((item) async {
        print("---->");
        try {
          final String reponse = item;
          final decodeData = json.decode(reponse);
          print(decodeData);
          var qrvalidation = await http.get(
              'http://192.168.43.141:9000/api/qrvalidation/' +
                  decodeData[0]['participant'].toString() +
                  '/' +
                  decodeData[0]['session'].toString());
          var qrBody = json.decode(qrvalidation.body);
          print(qrBody['data'][0]);
          //ID Creador
          if (decodeData[0]['createur'] == _user.id) {
            print("ES EL CREADOR CORRECTO");
            // EVENTO DISPONIBLE
            if (decodeData[0]['disponible'] == 1 &&
                qrBody['data'][0]['disponible'] == 1) {
              print("ESTÁ DISPONIBLE EL EVENTO");
              // Usuario Valido?
              if (decodeData[0]['valid'] == 0 &&
                  qrBody['data'][0]['validation'] == 0) {
                print("AÚN NO ESTÁ VALIDADO EL USUARIO");
                //Validar
                try {
                  await http.get(
                      'http://192.168.43.141:9000/api/presence/valider/' +
                          decodeData[0]['participant'].toString() +
                          '/' +
                          decodeData[0]['session'].toString());
                  _onAlertSucessValid(context, decodeData[0]['nom'].toString(),
                      decodeData[0]['prenom'].toString());
                } catch (e) {
                  print(e);
                }
              } else {
                print("YA SE VALIDÓ EL USUARIO");
                _onAlertErrorValid(context);
              }
            } else {
              print("NO ESTÁ DISPONIBLE EL EVENTO");
              _onAlertErrorDisponible(context);
            }
          } else {
            print("NO ERES CREADOR");
            _onAlertErrorCreateur(context);
          }
        } catch (e) {
          return e;
        }
      });
    } catch (e) {
      _onAlertError(context);
    }
  }

  Widget FadeAlertAnimation(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return Align(
      child: FadeTransition(
        opacity: animation,
        child: child,
      ),
    );
  }

  int currentTab = 0;

  final PageStorageBucket bucket = PageStorageBucket();

  @override
  Widget build(BuildContext context) {
    final List<Widget> screens = <Widget>[
      HomeFragment(user: _user),
      CalendarFragment(title: 'Table Calendar Demo', user: _user),
      TicketFragment(),
      ProfileFragment(
        user: _user,
      ),
    ];
    return Scaffold(
        body: SpinCircleBottomBarHolder(
            bottomNavigationBar: SCBottomBarDetails(
                iconTheme: IconThemeData(
                    color: Color.fromRGBO(18, 10, 110, 150), size: 30),
                activeIconTheme:
                    IconThemeData(color: Colors.pinkAccent, size: 30),
                circleColors: [
                  Colors.white,
                  Color.fromRGBO(0, 196, 204, 1),
                  Color.fromRGBO(76, 11, 209, 1)
                ],
                actionButtonDetails: SCActionButtonDetails(
                  color: Color.fromRGBO(228, 51, 151, 1),
                  icon: Icon(
                    Icons.domain_verification,
                    size: 30.0,
                  ),
                  elevation: 0,
                ),
                bnbHeight: 60,
                items: <SCBottomBarItem>[
                  SCBottomBarItem(
                      icon: Icons.home,
                      onPressed: () {
                        setState(() {
                          currentTab = 0;
                        });
                      }),
                  SCBottomBarItem(
                      icon: Icons.event,
                      onPressed: () {
                        setState(() {
                          currentTab = 1;
                        });
                      }),
                  SCBottomBarItem(
                      icon: Icons.confirmation_num,
                      onPressed: () {
                        setState(() {
                          currentTab = 2;
                        });
                      }),
                  SCBottomBarItem(
                      icon: Icons.person,
                      onPressed: () {
                        setState(() {
                          currentTab = 3;
                        });
                      }),
                ],
                circleItems: <SCItem>[
                  SCItem(
                      icon: Icon(
                        Icons.qr_code_rounded,
                        size: 15.0,
                        color: Color.fromRGBO(18, 10, 110, 150),
                      ),
                      onPressed: scanQR)
                ]),
            child: PageStorage(
              child: screens[currentTab],
              bucket: bucket,
            )));
  }

  _onAlertSucessValid(context, String nom, String prenom) {
    Alert(
      context: context,
      type: AlertType.success,
      title: nom + " " + prenom + " validée",
      desc:
          "Pour afficher les utilisateurs validés, ils se trouvent dans vos événements.",
      style: AlertStyle(descStyle: TextStyle(fontSize: 14)),
      buttons: [
        DialogButton(
          child: Text(
            "D'ACCORD",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () => Navigator.pop(context),
          gradient: LinearGradient(colors: [
            Color.fromRGBO(136, 49, 235, 1),
            Color.fromRGBO(253, 45, 248, 1),
          ]),
        )
      ],
      alertAnimation: FadeAlertAnimation,
    ).show();
  }

  _onAlertErrorCreateur(context) {
    Alert(
      context: context,
      type: AlertType.warning,
      title: "Createur incorrect",
      desc:
          "Nous sommes désolés, vous n’êtes pas le créateur de cet événement.",
      style: AlertStyle(descStyle: TextStyle(fontSize: 14)),
      buttons: [
        DialogButton(
          child: Text(
            "D'ACCORD",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () => Navigator.pop(context),
          gradient: LinearGradient(colors: [
            Color.fromRGBO(136, 49, 235, 1),
            Color.fromRGBO(253, 45, 248, 1),
          ]),
        )
      ],
      alertAnimation: FadeAlertAnimation,
    ).show();
  }

  _onAlertErrorDisponible(context) {
    Alert(
      context: context,
      type: AlertType.error,
      title: "Événement indisponible",
      desc: "Nous sommes désolés, cet événement n’est pas disponible.",
      style: AlertStyle(descStyle: TextStyle(fontSize: 14)),
      buttons: [
        DialogButton(
          child: Text(
            "D'ACCORD",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () => Navigator.pop(context),
          gradient: LinearGradient(colors: [
            Color.fromRGBO(136, 49, 235, 1),
            Color.fromRGBO(253, 45, 248, 1),
          ]),
        )
      ],
      alertAnimation: FadeAlertAnimation,
    ).show();
  }

  _onAlertErrorValid(context) {
    Alert(
      context: context,
      type: AlertType.error,
      title: "Personne déjà validée",
      desc: "Nous sommes désolés, cette personne a déjà été validée.",
      style: AlertStyle(descStyle: TextStyle(fontSize: 14)),
      buttons: [
        DialogButton(
          child: Text(
            "D'ACCORD",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () => Navigator.pop(context),
          gradient: LinearGradient(colors: [
            Color.fromRGBO(136, 49, 235, 1),
            Color.fromRGBO(253, 45, 248, 1),
          ]),
        )
      ],
      alertAnimation: FadeAlertAnimation,
    ).show();
  }

  _onAlertError(context) {
    Alert(
      context: context,
      type: AlertType.error,
      title: "QR n'est pas valable",
      desc: "Cette lecture de QR n’est pas valide, vérifie bien.",
      style: AlertStyle(descStyle: TextStyle(fontSize: 14)),
      buttons: [
        DialogButton(
          child: Text(
            "D'ACCORD",
            style: TextStyle(color: Colors.white, fontSize: 18),
          ),
          onPressed: () => Navigator.pop(context),
          gradient: LinearGradient(colors: [
            Color.fromRGBO(136, 49, 235, 1),
            Color.fromRGBO(253, 45, 248, 1),
          ]),
        )
      ],
      alertAnimation: FadeAlertAnimation,
    ).show();
  }
}
