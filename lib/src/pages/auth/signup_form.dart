import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:thevent/src/animation/fade_animation.dart';
import 'package:thevent/src/bloc/authentication_bloc/authentication_bloc.dart';
import 'package:thevent/src/bloc/authentication_bloc/authentication_event.dart';
import 'package:thevent/src/bloc/register_bloc/register_state.dart';
import 'package:thevent/src/bloc/register_bloc/register_bloc.dart';
import 'package:thevent/src/bloc/register_bloc/register_event.dart';
import 'package:thevent/src/size/size_config.dart';

class SignUpForm extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUpForm> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _nomController = TextEditingController();
  final TextEditingController _prenomController = TextEditingController();
  final TextEditingController _telephoneController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isButtonEnabled(RegisterState state) {
    return isPopulated && !state.isSubmitting;
  }

  RegisterBloc _registerBloc;

  @override
  void initState() {
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) {
        print("----------");
        print(state);
        print("----------");
        if (state.isFailure) {
          Scaffold.of(context)
            ..removeCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                  content: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Erreur lors de l'enregistrement"),
                      Icon(Icons.error),
                    ],
                  ),
                  backgroundColor: Colors.redAccent),
            );
        }

        if (state.isSuccess) {
          BlocProvider.of<AuthenticationBloc>(context).add(
            AuthenticationRegisterIn(),
          );
          Navigator.pop(context);
        }
      },
      child:
          BlocBuilder<RegisterBloc, RegisterState>(builder: (context, state) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: defaultSize * 3),
          child: Form(
            autovalidate: true,
            key: _formKey,
            child: Column(
              children: <Widget>[
                FadeAnimation(
                    1.5,
                    Text(
                      "Créer mon compte",
                      style: TextStyle(
                          color: Color.fromRGBO(18, 22, 64, 1),
                          fontSize: defaultSize * 2.5,
                          fontWeight: FontWeight.bold),
                    )),
                FadeAnimation(
                    1.6,
                    Container(
                      padding: EdgeInsets.all(defaultSize * .5),
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(18, 22, 64, 0),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        children: <Widget>[
                          nomField(context),
                          prenomField(context),
                          telephoneField(context),
                          emailField(context),
                          passwordField(context)
                        ],
                      ),
                    )),
                SizedBox(
                  height: defaultSize * 2,
                ),
                FadeAnimation(
                    1.7,
                    Container(
                        child: Center(
                      child: RaisedButton(
                        padding: EdgeInsets.all(0.0),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            _onFormSubmitted();
                          }
                        },
                        child: (state.isSubmitting || state.isSuccess)
                            ? Container(
                                height: defaultSize * 5,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    gradient: LinearGradient(colors: [
                                      Color.fromRGBO(49, 235, 228, 0.6),
                                      Color.fromRGBO(136, 49, 235, 0.6),
                                      Color.fromRGBO(253, 45, 248, 0.6),
                                    ])),
                                child: Center(
                                    child: SizedBox(
                                  height: defaultSize * 2,
                                  width: defaultSize * 2,
                                  child: CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white),
                                  ),
                                )),
                              )
                            : Container(
                                height: defaultSize * 5,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    gradient: LinearGradient(colors: [
                                      Color.fromRGBO(49, 235, 228, 1),
                                      Color.fromRGBO(136, 49, 235, 1),
                                      Color.fromRGBO(253, 45, 248, 1),
                                    ])),
                                child: Center(
                                  child: Text(
                                    "ENREGISTRER",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                      ),
                    ))),
              ],
            ),
          ),
        );
      }),
    );
  }

  Widget nomField(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Container(
      padding: EdgeInsets.all(defaultSize * .5),
      child: TextFormField(
        keyboardType: TextInputType.text,
        controller: _nomController,
        validator: MultiValidator([
          RequiredValidator(errorText: "Nécessaire *"),
        ]),
        decoration: InputDecoration(
          labelText: 'Nom',
          alignLabelWithHint: true,
          errorMaxLines: 1,
        ),
      ),
    );
  }

  Widget prenomField(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Container(
      padding: EdgeInsets.all(defaultSize * .5),
      child: TextFormField(
        keyboardType: TextInputType.text,
        controller: _prenomController,
        validator: MultiValidator([
          RequiredValidator(errorText: "Nécessaire *"),
        ]),
        decoration: InputDecoration(
          labelText: 'Prénom',
          alignLabelWithHint: true,
          errorMaxLines: 1,
        ),
      ),
    );
  }

  Widget telephoneField(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Container(
      padding: EdgeInsets.all(defaultSize * .5),
      child: TextFormField(
        keyboardType: TextInputType.phone,
        controller: _telephoneController,
        validator: MultiValidator([
          RequiredValidator(errorText: "Nécessaire *"),
          MaxLengthValidator(18, errorText: "Max 18 caractères")
        ]),
        decoration: InputDecoration(
          labelText: 'Téléphone',
          alignLabelWithHint: true,
          errorMaxLines: 1,
        ),
      ),
    );
  }

  Widget emailField(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Container(
      padding: EdgeInsets.all(defaultSize * .5),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        controller: _emailController,
        validator: MultiValidator([
          RequiredValidator(errorText: "Nécessaire *"),
          EmailValidator(errorText: "E-mail non valide")
        ]),
        decoration: InputDecoration(
          labelText: 'E-mail',
          alignLabelWithHint: true,
          errorMaxLines: 1,
        ),
      ),
    );
  }

  Widget passwordField(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Container(
      padding: EdgeInsets.all(defaultSize * .5),
      child: TextFormField(
        obscureText: true,
        controller: _passwordController,
        validator: MultiValidator([
          RequiredValidator(errorText: "Nécessaire *"),
          PatternValidator(r'(?=.*?[#?!@$%^&*-])',
              errorText: 'Au moins un caractère spécial'),
          MinLengthValidator(8,
              errorText: "Doit comporter au moins 8 caractères"),
        ]),
        decoration: InputDecoration(
          labelText: 'Mot de passe',
          alignLabelWithHint: true,
          errorMaxLines: 2,
        ),
      ),
    );
  }

  void _onFormSubmitted() {
    _registerBloc.add(RegisterSubmitted(
        nom: _nomController.text,
        prenom: _prenomController.text,
        telephone: _telephoneController.text,
        email: _emailController.text,
        password: _passwordController.text));
  }
}
