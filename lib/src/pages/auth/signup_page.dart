import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thevent/src/animation/fade_animation.dart';
import 'package:thevent/src/bloc/register_bloc/register_bloc.dart';
import 'package:thevent/src/pages/auth/signup_form.dart';
import 'package:thevent/src/repository/auth_repository.dart';
import 'package:thevent/src/size/size_config.dart';

class SignUp extends StatelessWidget {
  final AuthRepository _authRepository;

  const SignUp({Key key, AuthRepository authRepository})
      : _authRepository = authRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 255, 255, 1),
      body: BlocProvider<RegisterBloc>(
          create: (context) => RegisterBloc(authRepository: _authRepository),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                StackBack(),
                SizedBox(
                  height: defaultSize * 2,
                ),
                SignUpForm(),
              ],
            ),
          )),
    );
  }
}

class StackBack extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return FadeAnimation(
        1,
        Container(
          height: defaultSize * 20,
          width: defaultSize * 42,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage(
                'assets/images/background_login/signup/background_signup_2.png'),
            fit: BoxFit.fill,
          )),
          child: Stack(
            children: <Widget>[
              Positioned(
                height: defaultSize * 18,
                width: defaultSize * 42,
                child: FadeAnimation(
                    1.1,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                        image: AssetImage(
                            'assets/images/background_login/signup/background_signup_1.png'),
                        fit: BoxFit.fill,
                      )),
                    )),
              ),
              Positioned(
                height: defaultSize * 15,
                width: defaultSize * 42,
                child: FadeAnimation(
                    1.2,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/background_login/signup/background_signup_3.png'),
                              fit: BoxFit.fill)),
                    )),
              ),
              Positioned(
                height: defaultSize * 15,
                width: defaultSize * 42,
                child: FadeAnimation(
                    1.3,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/background_login/signup/background_signup_4.png'),
                              fit: BoxFit.fill)),
                    )),
              ),
              Positioned(
                height: defaultSize * 10,
                width: defaultSize * 42,
                child: FadeAnimation(
                    1.4,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/background_login/signup/background_signup_5.png'),
                              fit: BoxFit.fill)),
                    )),
              ),
            ],
          ),
        ));
  }
}
