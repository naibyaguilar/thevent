import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thevent/src/animation/fade_animation.dart';
import 'package:thevent/src/bloc/authentication_bloc/authentication_bloc.dart';
import 'package:thevent/src/bloc/authentication_bloc/authentication_event.dart';
import 'package:thevent/src/bloc/login_bloc/login_bloc.dart';
import 'package:thevent/src/bloc/login_bloc/login_event.dart';
import 'package:thevent/src/bloc/login_bloc/login_state.dart';
import 'package:thevent/src/pages/auth/signup_page.dart';
import 'package:thevent/src/repository/auth_repository.dart';
import 'package:thevent/src/size/size_config.dart';
import 'package:form_field_validator/form_field_validator.dart';

class LoginForm extends StatefulWidget {
  final AuthRepository _authRepository;

  const LoginForm({Key key, AuthRepository authRepository})
      : _authRepository = authRepository,
        super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isButtonEnabled(LoginState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  LoginBloc _loginBloc;

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return BlocListener<LoginBloc, LoginState>(listener: (context, state) {
      if (state.isFailure) {
        Scaffold.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Identification incorrecte"),
                    Icon(Icons.error),
                  ],
                ),
                backgroundColor: Colors.redAccent),
          );
      }
      if (state.isSuccess) {
        BlocProvider.of<AuthenticationBloc>(context).add(
          AuthenticationLoggedIn(),
        );
      }
    }, child: BlocBuilder<LoginBloc, LoginState>(builder: (context, state) {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: defaultSize * 3),
        child: Form(
          autovalidate: true,
          key: _formKey,
          child: Column(
            children: <Widget>[
              FadeAnimation(
                  1.6,
                  Text(
                    "Connectez-vous",
                    style: TextStyle(
                        color: Color.fromRGBO(18, 22, 64, 1),
                        fontSize: defaultSize * 2.5,
                        fontWeight: FontWeight.bold),
                  )),
              SizedBox(
                height: defaultSize * 3,
              ),
              FadeAnimation(
                  1.7,
                  Container(
                    padding: EdgeInsets.all(defaultSize * .5),
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(18, 22, 64, 0),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: <Widget>[
                        emailField(context),
                        passwordField(context)
                      ],
                    ),
                  )),
              SizedBox(
                height: defaultSize * 3,
              ),
              FadeAnimation(
                  1.8,
                  Container(
                      child: Center(
                    child: RaisedButton(
                      padding: EdgeInsets.all(0.0),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();
                          _login();
                        }
                      },
                      child: (state.isSubmitting || state.isSuccess)
                          ? Container(
                              height: defaultSize * 5,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  gradient: LinearGradient(colors: [
                                    Color.fromRGBO(49, 235, 228, 0.6),
                                    Color.fromRGBO(136, 49, 235, 0.6),
                                    Color.fromRGBO(253, 45, 248, 0.6),
                                  ])),
                              child: Center(
                                  child: SizedBox(
                                height: defaultSize * 2,
                                width: defaultSize * 2,
                                child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Colors.white),
                                ),
                              )),
                            )
                          : Container(
                              height: defaultSize * 5,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  gradient: LinearGradient(colors: [
                                    Color.fromRGBO(49, 235, 228, 1),
                                    Color.fromRGBO(136, 49, 235, 1),
                                    Color.fromRGBO(253, 45, 248, 1),
                                  ])),
                              child: Center(
                                child: Text(
                                  "ENTRER",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                    ),
                  ))),
              SizedBox(
                height: defaultSize * 3,
              ),
              Container(
                child: Column(
                  children: [
                    FadeAnimation(
                        1.9,
                        InkWell(
                          child: Text(
                            "Mot de passe oublié ?",
                            style: TextStyle(
                                fontSize: defaultSize * 1.5,
                                foreground: Paint()
                                  ..shader = LinearGradient(colors: <Color>[
                                    Color.fromRGBO(49, 235, 228, 1),
                                    Color.fromRGBO(136, 49, 235, 1),
                                  ]).createShader(
                                      Rect.fromLTWH(100, 0, 200, 0))),
                          ),
                          onTap: () {
                            Navigator.pushNamed(context, 'password');
                          },
                        )),
                    SizedBox(
                      height: defaultSize * 2,
                    ),
                    FadeAnimation(
                        2.0,
                        InkWell(
                          child: Text(
                            "Inscrivez-vous !",
                            style: TextStyle(
                                fontSize: defaultSize * 1.5,
                                foreground: Paint()
                                  ..shader = LinearGradient(colors: <Color>[
                                    Color.fromRGBO(136, 49, 235, 1),
                                    Color.fromRGBO(253, 45, 248, 1),
                                  ]).createShader(
                                      Rect.fromLTWH(100, 0, 200, 0))),
                          ),
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (_) {
                              return SignUp(
                                authRepository: widget._authRepository,
                              );
                            }));
                          },
                        )),
                    SizedBox(
                      height: defaultSize * 4,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      );
    }));
  }

  Widget emailField(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Container(
      padding: EdgeInsets.all(defaultSize * .5),
      child: TextFormField(
        keyboardType: TextInputType.emailAddress,
        controller: _emailController,
        validator: 
        MultiValidator([
          RequiredValidator(errorText: "Nécessaire *"),
          EmailValidator(errorText: "E-mail non valide")
        ]),
        decoration: InputDecoration(
          labelText: 'E-mail',
          alignLabelWithHint: true,
          errorMaxLines: 2,
        ),
      ),
    );
  }

  Widget passwordField(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Container(
      padding: EdgeInsets.all(defaultSize * .5),
      child: TextFormField(
        obscureText: true,
        controller: _passwordController,
        validator: MultiValidator([
          RequiredValidator(errorText: "Nécessaire *"),
          MinLengthValidator(8,
              errorText: "Doit comporter au moins 8 caractères"),
        ]),
        decoration: InputDecoration(
          labelText: 'Mot de Passe',
          alignLabelWithHint: true,
          errorMaxLines: 2,
        ),
      ),
    );
  }

  void _login() {
    _loginBloc.add(LoginWithCredentialsPressed(
        email: _emailController.text, password: _passwordController.text));
  }
}
