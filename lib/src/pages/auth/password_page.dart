import 'package:flutter/material.dart';
import 'package:thevent/src/animation/fade_animation.dart';
import 'package:thevent/src/size/size_config.dart';

class Password extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 255, 255, 1),
      body: Container(
          child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            FadeAnimation(
                1,
                Container(
                  height: defaultSize * 30,
                  width: defaultSize * 42,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                    image: AssetImage(
                        'assets/images/background_login/password/background_pwd_1.png'),
                    fit: BoxFit.fill,
                  )),
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        height: defaultSize * 28,
                        width: defaultSize * 42,
                        child: FadeAnimation(
                            1.1,
                            Container(
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                image: AssetImage(
                                    'assets/images/background_login/password/background_pwd_2.png'),
                                fit: BoxFit.fill,
                              )),
                            )),
                      ),
                      Positioned(
                        height: defaultSize * 30,
                        width: defaultSize * 42,
                        child: FadeAnimation(
                            1.2,
                            Container(
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/background_login/password/background_pwd_3.png'),
                                      fit: BoxFit.fill)),
                            )),
                      ),
                      Positioned(
                        height: defaultSize * 24,
                        width: defaultSize * 48,
                        child: FadeAnimation(
                            1.3,
                            Container(
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/background_login/password/background_pwd_4.png'),
                                      fit: BoxFit.fill)),
                            )),
                      ),
                      Positioned(
                        height: defaultSize * 22,
                        width: defaultSize * 42,
                        child: FadeAnimation(
                            1.4,
                            Container(
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/background_login/password/background_pwd_5.png'),
                                      fit: BoxFit.fill)),
                            )),
                      ),
                      Positioned(
                        height: defaultSize * 18,
                        width: defaultSize * 42,
                        child: FadeAnimation(
                            1.5,
                            Container(
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/background_login/password/background_pwd_6.png'),
                                      fit: BoxFit.fill)),
                            )),
                      ),
                    ],
                  ),
                )),
            SizedBox(
              height: defaultSize * 4,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: defaultSize * 3),
              child: Column(
                children: <Widget>[
                  FadeAnimation(
                      1.6,
                      Text(
                        "Mot de passe oublié?",
                        style: TextStyle(
                            color: Color.fromRGBO(18, 22, 64, 1),
                            fontSize: defaultSize * 2.5,
                            fontWeight: FontWeight.bold),
                      )),
                  SizedBox(
                    height: defaultSize,
                  ),
                  FadeAnimation(
                      1.7,
                      Text(
                        "Nous comprenons, des choses se passent. Il suffit d'entrer votre adresse e-mail ci-dessous et nous vous enverrons un lien pour réinitialiser votre mot de passe !",
                        style: TextStyle(color: Colors.grey, fontSize: defaultSize * 1.6),
                        textAlign: TextAlign.center,
                      )),
                  FadeAnimation(
                      1.8,
                      Container(
                        padding: EdgeInsets.all(defaultSize * .5),
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(18, 22, 64, 0),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(defaultSize * .5),
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color:
                                              Color.fromRGBO(18, 22, 64, 1)))),
                              child: TextField(
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "E-mail",
                                    hintStyle: TextStyle(
                                        color: Color.fromRGBO(18, 22, 64, .5), fontSize: defaultSize * 1.8)),
                              ),
                            ),
                          ],
                        ),
                      )),
                  SizedBox(
                    height: defaultSize * 5,
                  ),
                  FadeAnimation(
                      1.9,
                      Container(
                        height: defaultSize * 5,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            gradient: LinearGradient(colors: [
                              Color.fromRGBO(49, 235, 228, 1),
                              Color.fromRGBO(136, 49, 235, 1),
                              Color.fromRGBO(253, 45, 248, 1),
                            ])),
                        child: Center(
                          child: Text(
                            "SUIVANT",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ))
                ],
              ),
            )
          ],
        ),
      )),
    );
  }
}
