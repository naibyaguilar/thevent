//import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thevent/src/animation/fade_animation.dart';
import 'package:thevent/src/bloc/login_bloc/login_bloc.dart';
import 'package:thevent/src/pages/auth/login_form.dart';
import 'package:thevent/src/repository/auth_repository.dart';
import 'package:thevent/src/size/size_config.dart';

class Login extends StatelessWidget {
  final AuthRepository _authRepository;

  const Login({Key key, AuthRepository authRepository})
      : _authRepository = authRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 255, 255, 1),
      body: BlocProvider<LoginBloc>(
        create: (context) => LoginBloc(authRepository: _authRepository),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              StackBack(),
              LoginForm(
                authRepository: _authRepository,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

/*
class Login extends StatefulWidget {
  final AuthBloc authBloc;

  const Login({Key key, this.authBloc}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  AuthBloc get _authBloc => widget.authBloc;

  //device info
  /*DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  String _deviceName;*/

  @override
  void initState() {
    super.initState();
  }
/*
  void getDeviceName() async {
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidInfo = await deviceInfoPlugin.androidInfo;
        _deviceName = androidInfo.model;
      } else if (Platform.isIOS) {
        IosDeviceInfo iosInfo = await deviceInfoPlugin.iosInfo;
        _deviceName = iosInfo.utsname.machine;
      }
    } catch (e) {
      print(e);
    }
  }
*/

  @override
  Widget build(BuildContext context) {
    print('entras al login');
    return Scaffold(
      backgroundColor: Color.fromRGBO(255, 255, 255, 1),
      body: BlocProvider(
        create: (context) => _authBloc,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              StackBack(),
              LoginForm(
                authBloc: _authBloc,
              ),
              FooterInk(),
            ],
          ),
        ),
      ),
    );
  }
}
*/
class StackBack extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return FadeAnimation(
        1.1,
        Container(
          height: defaultSize * 35,
          decoration: BoxDecoration(
              image: DecorationImage(
            image: AssetImage(
                'assets/images/background_login/login/background_1.png'),
            fit: BoxFit.fill,
          )),
          child: Stack(
            children: <Widget>[
              Positioned(
                height: defaultSize * 30,
                width: defaultSize * 42,
                child: FadeAnimation(
                    1.2,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                        image: AssetImage(
                            'assets/images/background_login/login/background_2.png'),
                        fit: BoxFit.fill,
                      )),
                    )),
              ),
              Positioned(
                height: defaultSize * 35,
                width: defaultSize * 42,
                child: FadeAnimation(
                    1.3,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/background_login/login/background_4.png'),
                              fit: BoxFit.fill)),
                    )),
              ),
              Positioned(
                height: defaultSize * 22,
                width: defaultSize * 42,
                child: FadeAnimation(
                    1.4,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/background_login/login/background_3.png'),
                              fit: BoxFit.fill)),
                    )),
              ),
              Positioned(
                height: defaultSize * 15,
                width: defaultSize * 48,
                child: FadeAnimation(
                    1.5,
                    Container(
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/background_login/login/background_5.png'),
                              fit: BoxFit.fill)),
                    )),
              ),
            ],
          ),
        ));
  }
}

