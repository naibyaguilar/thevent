class Ticket {
  String message;
  List<TicketModel> tickets = new List();

  Ticket({this.message, this.tickets});

  Ticket.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    json['data'].forEach((value) {
      tickets.add(new TicketModel.fromJson(value));
    });
  }
}

class TicketModel {
  int session;
  String titreSession;
  String date;
  String heureDebut;
  String heureFin;
  int event;
  String image;
  String eventTitre;
  String location;
  String lat;
  String lng;
  int disponible;
  int participant;
  int validation;
  int createur;
  String nom;
  String prenom;

  TicketModel(
      {this.session,
      this.titreSession,
      this.date,
      this.heureDebut,
      this.heureFin,
      this.event,
      this.image,
      this.eventTitre,
      this.location,
      this.lat,
      this.lng,
      this.disponible,
      this.participant,
      this.validation,
      this.createur,
      this.nom,
      this.prenom});

  TicketModel.fromJson(Map<String, dynamic> json) {
    session = json['session'];
    titreSession = json['titreSession'];
    date = json['date'];
    heureDebut = json['heure_debut'];
    heureFin = json['heure_fin'];
    event = json['event'];
    image = json['image'];
    eventTitre = json['eventTitre'];
    location = json['location'];
    lat = json['lat'];
    lng = json['lng'];
    disponible = json['disponible'];
    participant = json['participant'];
    validation = json['validation'];
    createur = json['createur'];
    nom = json['nom'];
    prenom = json['prenom'];
  }

  getImage() {
    if (image == null) {
      return 'https://cdn.dribbble.com/users/1626229/screenshots/15031394/media/eef54ce87566d4217c4340a3049ff77c.jpg?compress=1&resize=1000x750';
    } else {
      return 'http://192.168.43.122:9000/storage/$image';
    }
  }
}

/*
class Data {
  List<EventTicket> events = new List();
  Createur createur;

  Data({this.events, this.createur});

  Data.fromJsonMap(Map<String, dynamic> json){
    json['events'].forEach((value){
      events.add(new EventTicket.fromJson(value));
    });
    createur = Createur.fromJson(json['createur']);
  }
}

class EventTicket {
  int id;
  String titre;
  String image;

  EventTicket({this.id, this.titre, this.image});
  EventTicket.fromJson(Map<String, dynamic> json){
    id = json['id'];
    titre = json['titre'];
    image = json['image'];
  }

  getImage(){
    if(image == null) {
      return 'https://cdn.dribbble.com/users/1626229/screenshots/15031394/media/eef54ce87566d4217c4340a3049ff77c.jpg?compress=1&resize=1000x750';
    }else{
      return 'http://192.168.43.122:9000/storage/$image';
    }
  }
}*/

class Createur {
  int id;
  String nom;
  String prenom;

  Createur({this.id, this.nom, this.prenom});
  Createur.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nom = json['nom'];
    prenom = json['prenom'];
  }
}
