class LoginAuth {
  String message;
  TokenAuth data;

  LoginAuth({this.message, this.data});
  LoginAuth.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    data = TokenAuth.fromJson(json['data']);
  }
}

class TokenAuth {
  String token;
  TokenAuth({this.token});

  TokenAuth.fromJson(Map<String, dynamic> data) {
    token = data['token'];
  }
}

class Register{
  String message;
  User data;

  Register({this.message, this.data});
  Register.fromJson(Map<String, dynamic> json){
    message = json['message'];
    data = User.fromJson(json['data']);
  }
}

class SignUp {
  String nom;
  String prenom;
  String telephone;
  String email;
  String password;

  SignUp({this.nom, this.prenom, this.telephone, this.email, this.password});
  SignUp.fromJson(Map<String, dynamic> json){
    if(json['data'] != null){
      nom = json['data']['nom'];
      prenom = json['data']['prenom'];
      email = json['data']['email'];
      telephone = json['data']['telephone'];
      password = json['data']['password'];
    } else {
      nom = "";
      prenom = "";
      email = "";
      telephone = "";
      password = "";
    }
  }
}


class User {
  int id;
  String nom;
  String prenom;
  String email;
  String telephone;

  User({this.id ,this.nom, this.prenom, this.email, this.telephone});

  User.fromJson(Map<String, dynamic> json){
    if(json['data'] != null){
      id = json['data']['id'];
      nom = json['data']['nom'];
      prenom = json['data']['prenom'];
      email = json['data']['email'];
      telephone = json['data']['telephone'];
    } else {
      nom = "";
      prenom = "";
      email = "";
      telephone = "";
    }
  }
  
}

class Logout{
  String message;

  Logout({this.message});

  Logout.fromJson(Map<String, dynamic> json){
    message = json['message'];
  }
}