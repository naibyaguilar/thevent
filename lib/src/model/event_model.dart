import 'package:thevent/src/model/session_model.dart';
import 'package:thevent/src/model/data_model.dart';

class Events {
  List<Event> items = new List();
  Events();

  Events.fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return;

    for (var item in jsonList) {
      final event = new Event.fromJsonMap(item);
      items.add(event);
    }
  }
}

class Event {
  int id;
  String titre;
  String description;
  String location;
  String lat;
  String lng;
  int disponible;
  String categorie;
  int createur;
  String image;
  String createdAt;
  String updatedAt;

  Event({
    this.id,
    this.titre,
    this.description,
    this.location,
    this.lat,
    this.lng,
    this.disponible,
    this.categorie,
    this.createur,
    this.image,
    this.createdAt,
    this.updatedAt,
  });

  Event.fromJsonMap(Map<String, dynamic> json) {
    id = json['id'];
    titre = json['titre'];
    description = json['description'];
    location = json['location'];
    lat = json['lat'];
    lng = json['lng'];
    disponible = json['disponible'];
    categorie = json['categorie'];
    createur = json['createur'];
    image = json['image'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  getImage() {
    if (image == null) {
      return 'https://cdn.dribbble.com/users/1626229/screenshots/15031394/media/eef54ce87566d4217c4340a3049ff77c.jpg?compress=1&resize=1000x750';
    } else {
      return 'http://192.168.43.122:9000/storage/$image';
    }
  }

  moonLanding() {
    var moonLanding = DateTime.parse(createdAt);
    return moonLanding;
  }
}

class EventDetail {
  Event event;
  List<User> createur = new List();
  List<Session> sessions = new List();
  int participants;
  EventDetail();

  EventDetail.fromJsonMap(Map<String, dynamic> jsonMap) {
    participants = jsonMap['participants'];
    jsonMap['sessions'].forEach((value) {
      sessions.add(new Session.fromJsonMap(value));
    });
    jsonMap['sessioncheck'].forEach((value) {
      for (var item in sessions) {
        if (item.id == value['session']) {
          item.isSelected = true;
        }
    }
    });
    event = Event.fromJsonMap(jsonMap['event']);
    createur.add(new User.fromJson(jsonMap['createur'][0]));
  }
}

class MyEventsDetail {
  List<Session> sessions = new List();
  int participants;
  MyEventsDetail({this.sessions, this.participants});

  MyEventsDetail.fromJsonMap(Map<String, dynamic> jsonMap) {
    participants = jsonMap['participants'];
    jsonMap['sessions'].forEach((value) {
      sessions.add(new Session.fromJsonMap(value));
    });
  }
}

class MyEvents {
  String message;
  List<Event> data = new List();

  MyEvents({this.message, this.data});

  MyEvents.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    json['data'].forEach((value) {
      data.add(new Event.fromJsonMap(value));
    });
  }
}

class MyParticipants {
  String message;
  List<Participant> data = new List();

  MyParticipants({this.message, this.data});

  MyParticipants.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    json['data'].forEach((value) {
      data.add(new Participant.fromJson(value));
    });
  }
}

class Participant {
  int id;
  String nom;
  String prenom;
  String telephone;

  Participant({this.id, this.nom, this.prenom, this.telephone});
  Participant.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nom = json['nom'];
    prenom = json['prenom'];
    telephone = json['telephone'];
  }
}

class ParticipantsSessions {
  String message;
  List<SessionsUsers> data = new List();

  ParticipantsSessions({this.message, this.data});

  ParticipantsSessions.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    json['data'].forEach((value) {
      data.add(new SessionsUsers.fromJson(value));
    });
  }
}

class SessionsUsers {
  String titre;
  String date;
  String heureDebut;
  String heureFin;
  int validation;

  SessionsUsers(
      {this.titre, this.date, this.heureDebut, this.heureFin, this.validation});

  SessionsUsers.fromJson(Map<String, dynamic> json) {
    titre = json['titre'];
    date = json['date'];
    heureDebut = json['heure_debut'];
    heureFin = json['heure_fin'];
    validation = json['validation'];
  }
}
