import 'package:flutter/material.dart';

class ChoiceChipData {
  final String label;
  final bool isSelected;
  final String icon;
  final String route;
  Color textColor;
  Color selectedColor;

  ChoiceChipData({
    @required this.label,
    @required this.isSelected,
    @required this.icon,
    @required this.route,
    @required this.textColor,
    @required this.selectedColor,
  });

  ChoiceChipData copy({
    String label,
    bool isSelected,
    String icon,
    String route,
    Color textColor,
    Color selectedColor,
  }) =>
      ChoiceChipData(
        label: label ?? this.label,
        isSelected: isSelected ?? this.isSelected,
        icon: icon ?? this.icon,
        route: route ?? this.route,
        textColor: textColor ?? this.textColor,
        selectedColor: selectedColor ?? this.selectedColor,
      );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ChoiceChipData &&
          runtimeType == other.runtimeType &&
          label == other.label &&
          isSelected == other.isSelected &&
          icon == other.icon &&
          route == other.route &&
          textColor == other.textColor &&
          selectedColor == other.selectedColor;

  @override
  int get hashCode =>
      label.hashCode ^
      isSelected.hashCode ^
      icon.hashCode ^
      route.hashCode ^
      textColor.hashCode ^
      selectedColor.hashCode;
}