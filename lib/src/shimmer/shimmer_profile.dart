import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:thevent/src/size/size_config.dart';

class ShimmerProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: defaultSize * 18, // 240
            child: Stack(
              children: <Widget>[
                ClipPath(
                  clipper: CustomShape(),
                  child: Container(
                    height: defaultSize * 15, //150
                    color: Color.fromRGBO(18, 22, 64, 1),
                    child: Image.asset(
                      'assets/images/background_profile.png',
                      width: defaultSize * 42,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: defaultSize),
                        height: defaultSize * 14,
                        width: defaultSize * 14,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: Colors.white, width: defaultSize * 0.8),
                            image: DecorationImage(
                                image: AssetImage(
                                    "assets/images/profile_image.png"),
                                fit: BoxFit.cover)),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Shimmer.fromColors(
            highlightColor: Colors.white,
            baseColor: Colors.grey[300],
            child: ShimmerLayout(),
            period: Duration(milliseconds: 800),
          ),
          Card(
            elevation: 2,
            margin: EdgeInsets.all(defaultSize * 2),
            child: Column(
              children: <Widget>[
                Shimmer.fromColors(
                  highlightColor: Colors.white,
                  baseColor: Colors.grey[300],
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      margin: EdgeInsets.all(defaultSize * 2),
                      height: defaultSize * 1.8,
                      width: defaultSize * 16,
                      color: Colors.grey,
                    ),
                  ),
                  period: Duration(milliseconds: 800),
                ),
                Divider(),
                Shimmer.fromColors(
                  highlightColor: Colors.white,
                  baseColor: Colors.grey[300],
                  child: ShimmerLayoutInfo(),
                  period: Duration(milliseconds: 800),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ShimmerLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Container(
      child: Column(
        children: [
          Container(
            height: defaultSize * 2.2,
            width: defaultSize * 20,
            color: Colors.grey,
          ),
          SizedBox(
            height: defaultSize * 2,
          ),
          Column(children: [
            Row(
              children: <Widget>[
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: defaultSize * 1.4,
                        width: defaultSize * 2.4,
                        color: Colors.grey,
                      ),
                      SizedBox(
                        height: defaultSize * 1,
                      ),
                      Container(
                        height: defaultSize * 1.6,
                        width: defaultSize * 5.4,
                        color: Colors.grey,
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: defaultSize * 1.4,
                        width: defaultSize * 2.4,
                        color: Colors.grey,
                      ),
                      SizedBox(
                        height: defaultSize * 1,
                      ),
                      Container(
                        height: defaultSize * 1.6,
                        width: defaultSize * 5.4,
                        color: Colors.grey,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ]),
          SizedBox(
            height: defaultSize * 4,
          ),
        ],
      ),
    );
  }
}

class ShimmerLayoutInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Container(
        child: Column(
      children: [
        ListTile(
          title: Container(
            height: defaultSize * 1.6,
            width: defaultSize * 5.4,
            color: Colors.grey,
          ),
          subtitle: Container(
            height: defaultSize * 1.4,
            width: defaultSize * 5.4,
            color: Colors.grey,
          ),
          leading: Container(
            height: defaultSize * 3,
            width: defaultSize * 3,
            color: Colors.grey,
          ),
        ),
        ListTile(
          title: Container(
            height: defaultSize * 1.6,
            width: defaultSize * 5.4,
            color: Colors.grey,
          ),
          subtitle: Container(
            height: defaultSize * 1.4,
            width: defaultSize * 5.4,
            color: Colors.grey,
          ),
          leading: Container(
            height: defaultSize * 3,
            width: defaultSize * 3,
            color: Colors.grey,
          ),
        ),
      ],
    ));
  }
}
class CustomShape extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    double height = size.height;
    double width = size.width;
    path.lineTo(0, height - 100);
    path.quadraticBezierTo(width / 2, height, width, height - 100);
    path.lineTo(width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
