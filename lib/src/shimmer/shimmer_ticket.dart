import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:thevent/src/size/size_config.dart';

class ShimmerTicket extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Shimmer.fromColors(
            highlightColor: Colors.white,
            baseColor: Colors.grey[300],
            child: ShimmerLayout(),
            period: Duration(milliseconds: 800),
          ),
        ],
      ),
    );
  }
}

class ShimmerLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Column(children: [
      ListTile(
        contentPadding: EdgeInsets.fromLTRB(
            defaultSize * 1, defaultSize * 2, defaultSize * 1, defaultSize * 0),
        title: Container(
          height: defaultSize * 1.8,
          color: Colors.grey,
        ),
        subtitle: Container(
          margin: EdgeInsets.only(top: defaultSize * 0.5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: defaultSize * 1.4,
                    width: defaultSize * 8,
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: defaultSize * 0.5,
                  ),
                  Container(
                    height: defaultSize * 1.6,
                    width: defaultSize * 20,
                    color: Colors.grey,
                  ),
                ],
              ),
            ],
          ),
        ),
        leading: Container(
            width: defaultSize * 10,
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(10.0),
            )),
      ),
      Divider(
        indent: defaultSize * 1,
        endIndent: defaultSize * 1,
        thickness: 1,
        color: Colors.black,
      ),
      ListTile(
        contentPadding: EdgeInsets.fromLTRB(
            defaultSize * 1, defaultSize * 0, defaultSize * 1, defaultSize * 0),
        title: Container(
          height: defaultSize * 1.8,
          color: Colors.grey,
        ),
        subtitle: Container(
          margin: EdgeInsets.only(top: defaultSize * 0.5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: defaultSize * 1.4,
                    width: defaultSize * 8,
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: defaultSize * 0.5,
                  ),
                  Container(
                    height: defaultSize * 1.6,
                    width: defaultSize * 20,
                    color: Colors.grey,
                  ),
                ],
              ),
            ],
          ),
        ),
        leading: Container(
            width: defaultSize * 10,
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(10.0),
            )),
      ),
      Divider(
        indent: defaultSize * 1,
        endIndent: defaultSize * 1,
        thickness: 1,
        color: Colors.black,
      ),
      ListTile(
        contentPadding: EdgeInsets.fromLTRB(
            defaultSize * 1, defaultSize * 0, defaultSize * 1, defaultSize * 0),
        title: Container(
          height: defaultSize * 1.8,
          color: Colors.grey,
        ),
        subtitle: Container(
          margin: EdgeInsets.only(top: defaultSize * 0.5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: defaultSize * 1.4,
                    width: defaultSize * 8,
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: defaultSize * 0.5,
                  ),
                  Container(
                    height: defaultSize * 1.6,
                    width: defaultSize * 20,
                    color: Colors.grey,
                  ),
                ],
              ),
            ],
          ),
        ),
        leading: Container(
            width: defaultSize * 10,
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(10.0),
            )),
      ),
      Divider(
        indent: defaultSize * 1,
        endIndent: defaultSize * 1,
        thickness: 1,
        color: Colors.black,
      ),
      ListTile(
        contentPadding: EdgeInsets.fromLTRB(
            defaultSize * 1, defaultSize * 0, defaultSize * 1, defaultSize * 0),
        title: Container(
          height: defaultSize * 1.8,
          color: Colors.grey,
        ),
        subtitle: Container(
          margin: EdgeInsets.only(top: defaultSize * 0.5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: defaultSize * 1.4,
                    width: defaultSize * 8,
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: defaultSize * 0.5,
                  ),
                  Container(
                    height: defaultSize * 1.6,
                    width: defaultSize * 20,
                    color: Colors.grey,
                  ),
                ],
              ),
            ],
          ),
        ),
        leading: Container(
            width: defaultSize * 10,
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(10.0),
            )),
      ),
      Divider(
        indent: defaultSize * 1,
        endIndent: defaultSize * 1,
        thickness: 1,
        color: Colors.black,
      ),
      ListTile(
        contentPadding: EdgeInsets.fromLTRB(
            defaultSize * 1, defaultSize * 0, defaultSize * 1, defaultSize * 0),
        title: Container(
          height: defaultSize * 1.8,
          color: Colors.grey,
        ),
        subtitle: Container(
          margin: EdgeInsets.only(top: defaultSize * 0.5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: defaultSize * 1.4,
                    width: defaultSize * 8,
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: defaultSize * 0.5,
                  ),
                  Container(
                    height: defaultSize * 1.6,
                    width: defaultSize * 20,
                    color: Colors.grey,
                  ),
                ],
              ),
            ],
          ),
        ),
        leading: Container(
            width: defaultSize * 10,
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(10.0),
            )),
      ),
    ]);
  }
}

class CustomShape extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    double height = size.height;
    double width = size.width;
    path.lineTo(0, height - 100);
    path.quadraticBezierTo(width / 2, height, width, height - 100);
    path.lineTo(width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
