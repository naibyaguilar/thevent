import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:thevent/src/size/size_config.dart';

class ShimmerParticipants extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Shimmer.fromColors(
            highlightColor: Colors.white,
            baseColor: Colors.grey[300],
            child: ShimmerLayout(),
            period: Duration(milliseconds: 800),
          ),
        ],
      ),
    );
  }
}

class ShimmerLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Column(children: [
      ListTile(
        title: Container(
          height: defaultSize * 1.8,
          color: Colors.grey,
        ),
        trailing: Container(
          width: defaultSize * 2.5,
          height: defaultSize * 2.5,
          decoration: BoxDecoration(
              color: Colors.grey, borderRadius: BorderRadius.circular(50)),
        ),
        subtitle: Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: defaultSize * 1.8,
                width: defaultSize * 8,
                color: Colors.grey,
              ),
              SizedBox(
                width: defaultSize * 2,
              ),
              Container(
                height: defaultSize * 1.8,
                width: defaultSize * 10,
                decoration: BoxDecoration(
                    color: Colors.grey, borderRadius: BorderRadius.circular(5)),
              ),
            ],
          ),
        ),
        leading: Container(
            child: CircleAvatar(),
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(50.0),
            )),
      ),
      Divider(
        indent: defaultSize * 8,
        endIndent: defaultSize * 1,
        thickness: 1,
        color: Colors.black,
      ),
      ListTile(
        title: Container(
          height: defaultSize * 1.8,
          color: Colors.grey,
        ),
        trailing: Container(
          width: defaultSize * 2.5,
          height: defaultSize * 2.5,
          decoration: BoxDecoration(
              color: Colors.grey, borderRadius: BorderRadius.circular(50)),
        ),
        subtitle: Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: defaultSize * 1.8,
                width: defaultSize * 8,
                color: Colors.grey,
              ),
              SizedBox(
                width: defaultSize * 2,
              ),
              Container(
                height: defaultSize * 1.8,
                width: defaultSize * 10,
                decoration: BoxDecoration(
                    color: Colors.grey, borderRadius: BorderRadius.circular(5)),
              ),
            ],
          ),
        ),
        leading: Container(
            child: CircleAvatar(),
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(50.0),
            )),
      ),
      Divider(
        indent: defaultSize * 8,
        endIndent: defaultSize * 1,
        thickness: 1,
        color: Colors.black,
      ),
      ListTile(
        title: Container(
          height: defaultSize * 1.8,
          color: Colors.grey,
        ),
        trailing: Container(
          width: defaultSize * 2.5,
          height: defaultSize * 2.5,
          decoration: BoxDecoration(
              color: Colors.grey, borderRadius: BorderRadius.circular(50)),
        ),
        subtitle: Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: defaultSize * 1.8,
                width: defaultSize * 8,
                color: Colors.grey,
              ),
              SizedBox(
                width: defaultSize * 2,
              ),
              Container(
                height: defaultSize * 1.8,
                width: defaultSize * 10,
                decoration: BoxDecoration(
                    color: Colors.grey, borderRadius: BorderRadius.circular(5)),
              ),
            ],
          ),
        ),
        leading: Container(
            child: CircleAvatar(),
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(50.0),
            )),
      ),
      Divider(
        indent: defaultSize * 8,
        endIndent: defaultSize * 1,
        thickness: 1,
        color: Colors.black,
      ),
      ListTile(
        title: Container(
          height: defaultSize * 1.8,
          color: Colors.grey,
        ),
        trailing: Container(
          width: defaultSize * 2.5,
          height: defaultSize * 2.5,
          decoration: BoxDecoration(
              color: Colors.grey, borderRadius: BorderRadius.circular(50)),
        ),
        subtitle: Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: defaultSize * 1.8,
                width: defaultSize * 8,
                color: Colors.grey,
              ),
              SizedBox(
                width: defaultSize * 2,
              ),
              Container(
                height: defaultSize * 1.8,
                width: defaultSize * 10,
                decoration: BoxDecoration(
                    color: Colors.grey, borderRadius: BorderRadius.circular(5)),
              ),
            ],
          ),
        ),
        leading: Container(
            child: CircleAvatar(),
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(50.0),
            )),
      ),
      Divider(
        indent: defaultSize * 8,
        endIndent: defaultSize * 1,
        thickness: 1,
        color: Colors.black,
      ),
      ListTile(
        title: Container(
          height: defaultSize * 1.8,
          color: Colors.grey,
        ),
        trailing: Container(
          width: defaultSize * 2.5,
          height: defaultSize * 2.5,
          decoration: BoxDecoration(
              color: Colors.grey, borderRadius: BorderRadius.circular(50)),
        ),
        subtitle: Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: defaultSize * 1.8,
                width: defaultSize * 8,
                color: Colors.grey,
              ),
              SizedBox(
                width: defaultSize * 2,
              ),
              Container(
                height: defaultSize * 1.8,
                width: defaultSize * 10,
                decoration: BoxDecoration(
                    color: Colors.grey, borderRadius: BorderRadius.circular(5)),
              ),
            ],
          ),
        ),
        leading: Container(
            child: CircleAvatar(),
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(50.0),
            )),
      ),
    ]);
  }
}

class CustomShape extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    double height = size.height;
    double width = size.width;
    path.lineTo(0, height - 100);
    path.quadraticBezierTo(width / 2, height, width, height - 100);
    path.lineTo(width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
