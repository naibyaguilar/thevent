import 'package:flutter/material.dart';

final _icons = <String, IconData>{
  'view_module': Icons.view_module,
  'account_balance': Icons.account_balance,
  'celebration': Icons.celebration,
  'music_note': Icons.music_note,
  'sports_soccer': Icons.sports_soccer,
  'class_': Icons.class_,
  'brush': Icons.brush,
  'euro': Icons.euro,
  'eco': Icons.eco,
  'laptop_mac': Icons.laptop_mac,
  'code': Icons.code,
  'loyalty': Icons.loyalty,
  'dinner_dining': Icons.dinner_dining,
  'spa': Icons.spa
};

Icon getIcon(String nameIcon, Color color) {
  return Icon(_icons[nameIcon], color: color);
}
