import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:thevent/src/model/data_model.dart';
import 'package:thevent/src/pages/navigation/navigation_page.dart';

class SliderScreen extends StatefulWidget {
  final User user;
  SliderScreen({Key key, this.user}) : super(key: key);

  @override
  SliderScreenState createState() => new SliderScreenState();
}

class SliderScreenState extends State<SliderScreen> {
  List<Slide> slides = new List();
  User get _user => widget.user;

  @override
  void initState() {
    super.initState();
    slides.add(
      new Slide(
        title: "ÉVÉNEMENTS",
        pathImage: "assets/images/slider/slider1.png",
        widthImage: 300.0,
        heightImage: 300.0,
        description:
            "Trouvez et participez à vos événements préférés situés près de chez vous !",
        backgroundColor: Color.fromRGBO(157, 75, 250, 1),
      ),
    );
    slides.add(
      new Slide(
        title: "CALENDRIER",
        pathImage: "assets/images/slider/slider2.png",
        widthImage: 300.0,
        heightImage: 300.0,
        description:
            "Tu n’oublieras jamais tes prochains événements avec notre super calendrier !",
        backgroundColor: Color.fromRGBO(69, 221, 230, 1),
      ),
    );
    slides.add(
      new Slide(
        title: "CREATEUR",
        pathImage: "assets/images/slider/slider3-1.png",
        widthImage: 300.0,
        heightImage: 300.0,
        description:
            "En outre, en tant que créateur d’un événement, vous pouvez gérer la présence de vos participants !",
        backgroundColor: Color.fromRGBO(247, 79, 177, 1),
      ),
    );
    slides.add(
      new Slide(
        title: "DÉCOUVRIR PLUS...",
        pathImage: "assets/images/slider/slider4.png",
        widthImage: 300.0,
        heightImage: 300.0,
        description:
            "Viens à découvrir notre application innovante pour la gestion d’événements sur TH-EVENT !",
        backgroundColor: Color.fromRGBO(255, 38, 92, 1),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IntroSlider(
          slides: this.slides,
          renderSkipBtn: new Text(
            "PASSER",
            style: TextStyle(color: Colors.white),
          ),
          renderNextBtn: new Icon(Icons.arrow_forward_ios_outlined,
              color: Colors.white, size: 30),
          renderDoneBtn: new Icon(
            Icons.check_circle,
            color: Colors.white,
            size: 35,
          ),
          onDonePress: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => NavigationPage(
                          user: _user,
                        )));
          }),
    );
  }
}
