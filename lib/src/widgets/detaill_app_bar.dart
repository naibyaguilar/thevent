import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:thevent/src/pages/home/fragments/myevent_participants_fragment.dart';

Widget appBarDetail(BuildContext context, double defaultSize, String image, int part, int evid) {
  return SliverAppBar(
    backgroundColor: Colors.white,
    expandedHeight: 200.0,
    floating: false,
    pinned: true,
    elevation: 0.0,
    iconTheme: IconThemeData(color: Colors.white),
    flexibleSpace: Stack(children: <Widget>[
      CachedNetworkImage(
        imageUrl: image,
        imageBuilder: (context, imageProvider) => Container(
            decoration: BoxDecoration(
                image: DecorationImage(fit: BoxFit.cover, image: imageProvider),
                borderRadius: BorderRadius.all(Radius.circular(20.0)))),
        placeholder: (context, url) => CircularProgressIndicator(),
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),
      Positioned(
          top: defaultSize * 5.0,
          right: defaultSize * 1.6,
          child: FittedBox(
            fit: BoxFit.scaleDown,
            alignment: Alignment.topLeft,
            child: GestureDetector(
              onTap: ()=> Navigator.push(context, MaterialPageRoute(builder: (context) => MyEventParticipant(id: evid, view: false,))),
              child: Container(
                  child: Row(
                    children: [
                      Icon(Icons.people_alt,
                          size: defaultSize * 2, color: Color(0xFFFFFFFF)),
                      Text('$part',
                          style: TextStyle(
                            color:Color(0xFFFFFFFF) ,
                              fontWeight: FontWeight.w300,
                              fontSize: defaultSize * 1.8))
                    ],
                  ),
                  width: defaultSize * 4.0,
                  height: defaultSize * 4.0,
                  decoration: BoxDecoration(
                      color: Colors.black26,
                      borderRadius: BorderRadius.all(Radius.circular(20.0)))),
            ),
          ))
    ]),
  );
}
