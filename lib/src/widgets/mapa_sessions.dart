import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:thevent/src/size/size_config.dart';
import 'package:thevent/src/widgets/nav_app_bar.dart';

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Scaffold(
        appBar: AppBar(
        flexibleSpace: backApp(defaultSize, ''),
        elevation: 2,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        centerTitle: true,
        title: Text("MES ÉVÉNEMENTS",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: defaultSize * 2.8,
                color: Colors.black)),
      ),
        body: FlutterMap(
    options: MapOptions(
      center: LatLng(51.5, -0.09),
      zoom: 13.0,
    ),
    layers: [
      MarkerLayerOptions(
        markers: [
          Marker(
            width: 80.0,
            height: 80.0,
            point: LatLng(48.2908852, 6.9408422),
            builder: (ctx) =>
            Container(
              child: Icon(Icons.graphic_eq_rounded, 
                    color: Color(0xFF7900FF),),
            ),
          ),
        ],
      ),
    ],
    children: <Widget>[
      TileLayerWidget(options: TileLayerOptions(
        urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        subdomains: ['a', 'b', 'c']
      )),
      MarkerLayerWidget(options: MarkerLayerOptions(
        markers: [
          Marker(
            width: 80.0,
            height: 80.0,
            point: LatLng(48.2908852,6.9408422),
            builder: (ctx) =>
            Container(
              child: Icon(Icons.graphic_eq_rounded, 
                    color: Color(0xFF7900FF),),
            ),
          ),
        ],
      )),
    ],
  ));
  }
}

