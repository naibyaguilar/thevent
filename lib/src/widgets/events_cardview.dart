import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:date_format/date_format.dart';

import 'package:flutter/material.dart';
import 'package:thevent/src/model/event_model.dart';
import 'package:thevent/src/model/data_model.dart';
import 'package:thevent/src/pages/detail_event_page.dart';
import 'package:thevent/src/size/size_config.dart';

class EventCardView extends StatelessWidget {
   
  @override
  final User user;
  final List<Event> event;
  EventCardView({@required this.event, this.user});
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return Expanded(
      child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: event.length,
          padding:
              EdgeInsets.only(left: defaultSize*1, top: defaultSize*1, right: defaultSize*1, bottom: 0.0),
          itemBuilder: (context, i) =>
              _viewHolderEvent(context, i, event, defaultSize)),
    );
  }

  Widget _viewHolderEvent(BuildContext context, int i, List<Event> event, double defaultSize) {
    return GestureDetector(
      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailEvent(id: event[i].id, user: user))),
      child: ClipRect(
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: defaultSize*1, sigmaY: defaultSize*1),
          child: Container(
            height: defaultSize * 22.0,
            margin:
                EdgeInsets.only(left: defaultSize*1, top: defaultSize*1, right: defaultSize*1, bottom: defaultSize*1),
            decoration: BoxDecoration(
              color: Color(0xFFFFFFFF),
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: [BoxShadow(color: Colors.black54, blurRadius: 2.0)],
            ),
            child: Stack(
              children: <Widget>[
                _mediaEvent(defaultSize, event[i].getImage()),
                _dateEvent(defaultSize, event[i].moonLanding() ),
                _texts(context, i, event, defaultSize)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _texts(
      BuildContext context, int i, List<Event> event, double defaultSize) {
    return SafeArea(
      child: Column(
        children: <Widget>[
          SizedBox(height: defaultSize * 2.0),
          Expanded(child: Container()),
          _subheadEvent(context, i, event, defaultSize)
        ],
      ),
    );
  }

  Widget _mediaEvent(double defaultSize, String imagee) {
    return SizedBox(
      height: defaultSize * 14.0,
      child: Container(
        child: CachedNetworkImage(
          imageUrl: imagee,
          imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
                image: DecorationImage(fit: BoxFit.cover, image: imageProvider),
                borderRadius: BorderRadius.all(Radius.circular(20.0)))),
          placeholder: (context, url) => CircularProgressIndicator(),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),
      ),
    );
  }

  Widget _dateEvent(double defaultSize, DateTime moonLanding) {
    return Positioned(
        bottom: defaultSize * 7.0,
        left: defaultSize * 1.6,
        right: defaultSize * 1.6,
        child: FittedBox(
          fit: BoxFit.scaleDown,
          alignment: Alignment.topLeft,
          child: SizedBox(
            width: defaultSize * 6.5,
            child: Container(
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(color: Colors.black54, blurRadius: 2.0)
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(moonLanding.day.toString(),
                      style: TextStyle(
                          fontSize: defaultSize * 4.0,
                          color: Color(0xFF3A2DB3))),
                  Text(formatDate(moonLanding, [M]),
                      style: TextStyle(
                          fontSize: defaultSize * 1.5,
                          color: Color(0xFF121640))),
                  SizedBox(height: defaultSize * .5),
                ],
              ),
            ),
          ),
        ));
  }

  Widget _subheadEvent(
      BuildContext context, int i, List<Event> event, double defaultSize) {
    return ListTile(
      title: Text(event[i].titre,
          style: TextStyle(fontSize: defaultSize * 2.6, color: Colors.black)),
      subtitle: Text(
        event[i].categorie,
        style: TextStyle(color: Colors.black.withOpacity(0.6)),
      ),
      trailing: Icon(Icons.remove_red_eye_outlined, color: Color(0xFF998DDB)),
      isThreeLine: false,
    );
  }
}
