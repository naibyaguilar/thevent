import 'dart:math';

import 'package:flutter/material.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

Widget backApp(double defaultSize, String tilte) {
    final waves = Transform.rotate(
        angle: -pi / 1, //rotate
        child: Container(
            child: WaveWidget(
          duration: 1,
          config: CustomConfig(
            gradients: [
              [Color(0xFF3A2DB3), Color(0xFF3A2DB1)],
              [Color(0xFFEC72EE), Color(0xFFFF7D9C)],
              [Color(0xFFfc00ff), Color(0xFF00dbde)],
              [Color(0xFF396afc), Color(0xFF2948ff)]
            ],
            durations: [35000, 19440, 10800, 6000],
            heightPercentages: [defaultSize* .01, defaultSize* 0.023, defaultSize* 0.025, defaultSize*0.030],
            blur: MaskFilter.blur(BlurStyle.inner, 5),
            gradientBegin: Alignment.centerLeft,
            gradientEnd: Alignment.centerRight,
          ),
          waveAmplitude: defaultSize *5.0,
          backgroundColor: Colors.white,
          size: Size(double.infinity, defaultSize*5.0), //size
        )));

    return  Stack(children:[waves, Center(
      child: Text(tilte, style: TextStyle(fontWeight: FontWeight.bold, fontSize: defaultSize* 2.8, color: Colors.black)),
    ),] );
  }

