import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:thevent/src/pages/home/fragments/ticket_details_fragment.dart';
import 'package:thevent/src/services/ticket_services.dart';
import 'package:thevent/src/shimmer/shimmer_ticket.dart';
import 'package:thevent/src/size/size_config.dart';

class TicketPassesListView extends StatelessWidget {
  final ticket = new TicketService();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    double defaultSize = SizeConfig.defaultSize;
    return FutureBuilder(
        future: ticket.getTicketsIndisponible(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.separated(
              padding: EdgeInsets.only(top: defaultSize * 1),
              separatorBuilder: (context, index) => Divider(
                indent: defaultSize * 1,
                endIndent: defaultSize * 1,
                thickness: 1,
              ),
              itemCount: snapshot.data.length,
              itemBuilder: (_, index) {
                return ListTile(
                  contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                  title: Container(
                      child: Row(
                    children: [
                      Icon(
                        Icons.circle,
                        color: Colors.pinkAccent[400],
                        size: defaultSize * 1,
                      ),
                      Flexible(
                        child: Container(
                          margin: EdgeInsets.only(left: defaultSize * .5),
                          child: Text(snapshot.data[index].titreSession,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: defaultSize * 1.8)),
                        ),
                      ),
                    ],
                  )),
                  leading: Container(
                    width: defaultSize * 10,
                    child: CachedNetworkImage(
                      imageUrl: snapshot.data[index].getImage(),
                      imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                        image: DecorationImage(
                            fit: BoxFit.fitWidth, image: imageProvider),
                        borderRadius: BorderRadius.circular(10.0),
                      )),
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                  trailing: IconButton(
                    icon: Icon(
                      Icons.arrow_forward_ios_rounded,
                      color: Color(0xFF998DDB),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DetailTicket(
                                    ticketModel: snapshot.data[index],
                                  )));
                    },
                  ),
                  subtitle: Container(
                    margin: EdgeInsets.only(top: defaultSize * 0.5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Date",
                                style: TextStyle(
                                    fontSize: defaultSize * 1.4,
                                    color: Colors.grey),
                              ),
                              Text(
                                snapshot.data[index].date,
                                style: TextStyle(
                                    fontSize: defaultSize * 1.6,
                                    color: Colors.black),
                              )
                            ],
                          ),
                        ),
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Heure",
                                style: TextStyle(
                                    fontSize: defaultSize * 1.4,
                                    color: Colors.grey),
                              ),
                              Text(
                                snapshot.data[index].heureDebut,
                                style: TextStyle(
                                    fontSize: defaultSize * 1.6,
                                    color: Colors.black),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          } else {
            return ShimmerTicket();
          }
        });
  }
}
