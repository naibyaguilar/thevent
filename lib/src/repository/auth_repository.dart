import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:thevent/src/model/data_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

final baseUrl = "http://192.168.43.122:9000/api";

class AuthRepository {
  Future loginUser(String _email, String _password, String _device) async {
    try {
      print("repository - loginUser - try");
      var response = await http
          .post('$baseUrl/auth/login', body: {
        'email': _email,
        'password': _password,
        'device_name': _device
      });

      var jsonResponse = json.decode(response.body);
      print(jsonResponse);
      return LoginAuth.fromJson(jsonResponse);
    } catch (e) {
      print("repository - loginUser - error catch");
      return e;
    }
  }

  Future userLogout(String token) async {
    try {
      print("repository - userLogout - try");
      var response = await http
          .post('$baseUrl/auth/logout', headers: {
        'Authorization': 'Bearer $token',
        'Accept': 'application/json'
      });

      var resBody = json.decode(response.body);
      return Logout.fromJson(resBody);
    } catch (e) {
      print("repository - userLogout - error catch");
      return e;
    }
  }

  Future getData(String token) async {
    try {
      print("repository - getData - try");
      var response = await http.get('$baseUrl/user',
          headers: {
            'Authorization': 'Bearer $token',
            'Accept': 'application/json'
          });

      var body = json.decode(response.body);
      return User.fromJson(body);
    } catch (e) {
      print("repository - getData - error catch");
      return e;
    }
  }

  Future signUp(String _nom, String _prenom, String _telephone, String _email,
      String _password) async {
    print(_nom);
    print(_prenom);
    print(_telephone);
    print(_email);
    print(_password);
    print("repository - register");
    try {
      print("repository - register - try");
      var response = await http.post(
        '$baseUrl/auth/register',
        body: {
          'nom': _nom,
          'prenom': _prenom,
          'telephone': _telephone,
          'email': _email,
          'password': _password,
        },
      );

      var jsonResponse = json.decode(response.body);
      print(jsonResponse);
      return Register.fromJson(jsonResponse);
    } catch (e) {
      print("repository - loginUser - error catch");
      return e;
    }
  }

  Future hasToken() async {
    print("repository - hasToken");
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences local = await _prefs;
    final String token = local.getString("token_sanctum") ?? null;
    if (token != null) return token;
    return null;
  }

  Future setLocalToken(String token) async {
    print("repository - setLocalToken");
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences local = await _prefs;
    local.setString("token_sanctum", token);
  }

  Future unsetLocalToken() async {
    print("repository - unsetLocalToken");
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences local = await _prefs;
    local.setString("token_sanctum", null);
  }
}
