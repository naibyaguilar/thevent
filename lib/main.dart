import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:thevent/src/bloc/authentication_bloc/authentication_bloc.dart';
import 'package:thevent/src/bloc/authentication_bloc/authentication_event.dart';
import 'package:thevent/src/bloc/authentication_bloc/authentication_state.dart';
import 'package:thevent/src/bloc/simple_bloc_observer.dart';
import 'package:thevent/src/repository/auth_repository.dart';
import 'package:thevent/src/pages/navigation/navigation_page.dart';
import 'package:thevent/src/pages/auth/login_page.dart';
import 'package:thevent/src/pages/auth/password_page.dart';
import 'package:thevent/src/pages/auth/signup_page.dart';
import 'package:thevent/src/widgets/intro_slider.dart';


void main() {
  Bloc.observer = SimpleBlocObserver();
  final AuthRepository authRepository = AuthRepository();

  runApp(BlocProvider(
    create: (context) => AuthenticationBloc(
      authRepository: authRepository,
    )..add(AuthenticationStarted()),
    child: App(
      authRepository: authRepository,
    ),
  ));
}

class App extends StatelessWidget {
  final AuthRepository _authRepository;

  App({AuthRepository authRepository}) : _authRepository = authRepository;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          builder: (context, state) {
            if (state is AuthenticationFailure) {
              print('main Faild');
              return Login(
                authRepository: _authRepository,
              );
            }
            if (state is AuthenticationSuccess) {
              print('main Success');
              return NavigationPage(
                user: state.user,
              );
            }
            if (state is AuthenticationRegisterSuccess) {
              print('main Success Register');
              return SliderScreen(
                user: state.user,
              );
            }
            print('main NO STATE');
            return Container(
              color: Colors.white,
              child: Center(
                child: CircularProgressIndicator(
                  strokeWidth: 6,
                  valueColor:
                      new AlwaysStoppedAnimation<Color>(Colors.pinkAccent),
                ),
              ),
            );
          },
        ),
        title: 'Th-Event',
        //initialRoute: 'login',
        routes: {
          'login': (BuildContext context) => Login(),
          'signup': (BuildContext context) => SignUp(),
          'password': (BuildContext context) => Password(),
          'navigation': (BuildContext context) => NavigationPage(),
        });
  }
}
